using System;
using System.IO;
using System.Web.Http;
using SistemaInfo.ConnectUs.Api;
using WebActivatorEx;
using Swashbuckle.Application;
using System.Reflection;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace SistemaInfo.ConnectUs.Api
{
    public class SwaggerConfig
    {
        public static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            if (args.Name.Contains("Newtonsoft.Json"))
            {
                if (args.Name.Contains("PublicKeyToken=null"))
                    return Assembly.LoadFrom(Path.Combine(System.Web.HttpRuntime.AppDomainAppPath, "Assemblies", "Newtonsoft.Json.SysMiddle", "Newtonsoft.Json.dll"));

                return Assembly.LoadFrom(Path.Combine(System.Web.HttpRuntime.AppDomainAppPath, "Assemblies", "Newtonsoft.Json", "Newtonsoft.Json.dll"));
            }
            else
                return Assembly.LoadFrom(String.Empty);
        }

        public static void Register()
        {
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);

            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {                                                
                        c.SingleApiVersion("v1", "Connect US - Conversão de layout");
                        c.DescribeAllEnumsAsStrings();
                        c.IncludeXmlComments(GetXmlCommentsPath());
                    })
                .EnableSwaggerUi();
        }

        private static string GetXmlCommentsPath()
        {
            return Path.Combine(AppContext.BaseDirectory, "App_Data", System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".xml");
        }
    }
}
