﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using SistemaInfo.ConnectUs.Api.Application.Filters;

namespace SistemaInfo.ConnectUs.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            //GlobalConfiguration.Configuration.Filters.Add(new RequestLogFilter());

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );            
        }
    }
}
