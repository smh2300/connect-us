﻿using System.IO;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SysMiddle.ConnectUs.API.Service;
using System.Reflection;
using System;
using SistemaInfo.ConnectUs.Api.Application.Filters;

namespace SistemaInfo.ConnectUs.Api
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            APIManager.GlobalConfigurationFileName = Path.Combine(HttpRuntime.AppDomainAppPath, "global.config");
        }
    }
}
