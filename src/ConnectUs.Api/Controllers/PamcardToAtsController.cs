using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml.Linq;
using Newtonsoft.Json;
using SistemaInfo.ConnectUs.Api.Application.ATS;
using SistemaInfo.ConnectUs.Api.Application.Objects;
using SistemaInfo.ConnectUs.Api.Application.SistemaInfoServices.Infra;
using SistemaInfo.ConnectUs.Api.Application.SysMiddle;
using SistemaInfo.ConnectUs.Functions.Pamcard;
using SistemaInfo.Framework.Utils;
using SistemaInfo.MicroServices.Rest.Infra.ApiClient;
using SysMiddle.ConnectUs.API.Service;
using SysMiddle.ConnectUs.Core.Interface.ValueObject.API;
using SysMiddle.ConnectUs.Core.Interface.ValueObject.GeneralObjects.Context;

// ----------------------
// Configrar variável de ambiente do windows
// Nome: ConnectUsFunciontsFolder
// Valor: Path onde a instalação do seu ConnectUs carrega as funções personalizadas
// Objetivo: Ao dar o build do projeto pelo VS/Rider, já será exportada a DLL para o seu diretório do ConnectUs
// ----------------------

namespace SistemaInfo.ConnectUs.Api.Controllers
{
    /// <summary>
    /// Recursos para conversão de layout Pamcard x ATS.
    /// Pamcard utiliza o padrão SOAP e tudo entrará através do método Execute(). Veja a documentação do mesmo para mais detalhes.
    /// No cliente deve ser modificado o link para: 
    /// </summary>
    [RoutePrefix("PamcardToAts")]    
    public class PamcardToAtsController : ApiController
    {                
        private APIExecutor ApiExecutor { get; } = Application.SysMiddle.ConnectUs.ApiExecutor;
        private AtsRepository AtsRepository;
        private PamcardDataManager PamcardManager { get; } = new PamcardDataManager();
        private InfraExternalRepository InfraRepository { get; } = new InfraExternalRepository();
        private string _cnpjContratante = null;
        private string _tokenMicroServices = null;
                
        // Método Execute interpreta o XML Pamcard e inicializa esta variável
        private string RequestXml = null;
        private ParserResultBasicVO _parserContext;

        /// <summary>
        /// Método de entrada de todas integrações via protocolo SOAP da Pamcard
        /// </summary>
        /// <remarks>
        /// Altere o link de integração da Pamcard da para este Proxy de # conversão de dados.
        /// ###### Link Pamcard original:
        ///     https://www.roadcard.com.br/sistemapamcard/services/WSTransacional
        ///     https://preproducao.roadcard.com.br/sistemapamcard/services/WSTransacional
        /// ##### Link de conversão de dados:
        ///     {url_base_ate_este_projeto}/PamcardToAts
        ///     Exemplo: https://atstechnology.com.br:4100/ConnectUs/PamcardToAts
        /// ---
        /// ##### Responsabilidades deste método:
        /// - Realizar a leitura da tag Envelope/Body/Execute/execute/arg0/context
        /// - Redirecionar a requisição para o método especialista específico (Exemplo: FindFavored, InsertFavored, InsertFavored, etc)
        /// - O parser já processado da ConnectUs Api deve ser repassado aos métodos especialias
        /// - Este método deve retornar o XML informado pelo método especialista
        /// - Este método deve registrar no micro serviço de log a operação SOAP (Request/Response)
        /// </remarks>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public async Task<HttpResponseMessage> Execute()
        {
            string result = null;
            LogIntegracaoInsertApiRequest log = null;
            try
            {
                if (ApiExecutor == null)
                    throw new Exception("Verificar licença do servidor" );
                
                using (var contentStream = await Request.Content.ReadAsStreamAsync())
                {
                    contentStream.Seek(0, SeekOrigin.Begin);
                    using (var sr = new StreamReader(contentStream))
                    {
                        RequestXml = sr.ReadToEnd();
                        PamcardManager.LoadXml(RequestXml);
                        LoadMetadata();

                        log = InfraRepository.LogInputRequest(_tokenMicroServices, PamcardManager.RequestContext,
                            RequestXml);

                        switch (PamcardManager.RequestContext)
                        {
                            case "FindFavored":
                                result = FindFavored();
                                break;
                            case "FindFavoredAccount":
                                result = FindFavoredAccount();
                                break;
                            case "InsertFavored":
                                result = InsertFavored("").ResultXml;
                                break;
                            case "InsertFreightContract":
                                result = InsertFreightContract();
                                break;
                            case "FindFleet":
                                result = FindFleet();
                                break;
                            case "CancelTrip":
                                result = CancelTrip();
                                break;
                            case "FindFreightContract":
                                result = FindFreightContract();
                                break;
                            case "CloseFreightContract":
                                result = CloseFreightContract();
                                break;
                            case "FindRNTRC":
                                result = FindRNTRC();
                                break;
                            case "PayParcel":
                                result = PayParcel();
                                break;
                            case "UpdateParcelStatus":
                                result = UpdateParcelStatus();
                                break;
                            case "Router":
                                result = Router();
                                break;
                            case "UpdateValuesFreightContract":
                                result = UpdateValuesFreightContract();
                                break;
                            case "InsertTrip":
                                result = InsertTrip();
                                break;
                            case "FindParcelStatus":
                                result = FindParcelStatus();
                                break;
                            case "InsertCardFreight":
                                result = InsertCardFreight();
                                break;
                            case "FindCard":
                                result = FindCard();
                                break;
                            case "FindTrip":
                                result = FindTrip();
                                break;
                            case "InsertParcel":
                                result = InsertParcel();
                                break;
                            default:
                                result = CriarXmlResposta("4",
                                    $"Método '{PamcardManager.RequestContext}' não suportado pelo conversor ATS x PEF.");
                                break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var message = e.Message;
                var baseMessage = e.GetBaseException()?.Message;
                if (baseMessage.HasValue() && !baseMessage.Equals(message))
                    message += ", " + baseMessage;
                
                result = CriarXmlResposta("4", "Erro inesperado: " + message);
            }
            finally
            {
                if (log != null && result != null)
                    InfraRepository.LogInputResponse(log.Id.GetValueOrDefault(), _tokenMicroServices, result);
            }

            return new HttpResponseMessage
            {
                Content = new StringContent(
                    result,
                    System.Text.Encoding.UTF8,
                    "text/xml"
                )
            };
        }

        private void LoadMetadata()
        {
            _cnpjContratante = PamcardManager.GetFieldValue("viagem.contratante.documento.numero");
            
            AtsRepository = new AtsRepository(_cnpjContratante);
            _tokenMicroServices = AtsRepository.GetTokenMicroServicos(_cnpjContratante);
        }

        /// <summary>
        /// Consultar favorecido
        /// </summary>
        /// <remarks>
        /// ### Pamcard
        /// - Consultar favorecido (Cadastro única de proprietário/motorista)
        /// O tipo de documeto para consulta é chaveado pela tag: viagem.favorecido.documento.tipo
        /// 1 - CNPJ
        /// 2 - CPF
        /// 5 e 6 - RNTRC
        /// ### ATS
        /// - Caso CNPJ
        ///   - Consultar na tabela de proprietário se existe.
        /// - Para CPF
        ///   - Retornar OK somente se existir em ambas as tabela de proprietário e motorista. 
        /// </remarks>
        /// <returns></returns>
        [HttpPost]
        [Route("FindFavored")]
        public string FindFavored()
        {            
            var cnpjEmpresa = PamcardManager.GetFieldValue("viagem.contratante.documento.numero");            
            var documento = PamcardManager.GetFieldValue("viagem.favorecido.documento.numero");
//            var tipo = PamcardManager.GetFieldValue("viagem.favorecido.documento.tipo");

            if (documento.Length <= 11)
            {
                var mappedMotorista = ConsultarMotorista(cnpjEmpresa, documento);
                var mappedProprietario = ConsultarProprietario(cnpjEmpresa, documento);

                if (mappedMotorista != null && mappedProprietario != null)
                    return mappedProprietario;

                var documentoResposta = CriarXmlResposta("4", "Favorecido não encontrado");

                return documentoResposta;
            }

            return ConsultarProprietario(cnpjEmpresa, documento);                    
        }
        
        [HttpPost]
        [Route("FindFavoredAccount")]
        public string FindFavoredAccount()
        {
            // Resposta fake. Método não faz nenhuma consulta
            XNamespace env = "http://schemas.xmlsoap.org/soap/envelope/";
            XNamespace ns1 = "http://webservice.pamcard.jee.pamcary.com.br";

            var documentoResposta = new XDocument(
                new XElement(env + "Envelope", new
                        XAttribute(XNamespace.Xmlns + "env", "http://schemas.xmlsoap.org/soap/envelope/"),
                    new XElement(env + "Header"),
                    new XElement(env + "Body",
                        new XElement(ns1 + "executeResponse",
                            new XAttribute(XNamespace.Xmlns + "ns1",
                                "http://webservice.pamcard.jee.pamcary.com.br"),
                            new XElement("return",
                                new XElement("fields",
                                    new XElement("key", "mensagem.codigo"), new XElement("value", "0")),
                                new XElement("fields",
                                    new XElement("key", "mensagem.descricao"), new XElement("value", "Operação realizada com sucesso")),
                                new XElement("fields",
                                    new XElement("key", "viagem.favorecido.documento.tipo"), new XElement("value", "79247920000169")),
                                new XElement("fields",
                                    new XElement("key", "viagem.favorecido.documento.numero"), new XElement("value", "1")),
                                new XElement("fields",
                                    new XElement("key", "viagem.favorecido.conta.agencia"), new XElement("value", "9999")),
                                new XElement("fields",
                                    new XElement("key", "viagem.favorecido.conta.banco"), new XElement("value", "001")),
                                new XElement("fields",
                                    new XElement("key", "viagem.favorecido.conta.numero"), new XElement("value", "99999")),
                                new XElement("fields",
                                    new XElement("key", "viagem.favorecido.conta.status"), new XElement("value", "3")),
                                new XElement("fields",
                                    new XElement("key", "viagem.favorecido.conta.tipo"), new XElement("value", "1"))
                            ))
                    ))
            );  
            
            return documentoResposta.ToString();
        }
        
        /// <summary>
        /// Consultar favorecido
        /// </summary>
        /// <remarks>
        /// ### Pamcard
        /// - Consultar favorecido (Cadastro única de proprietário/motorista)
        /// O tipo de documeto para consulta é chaveado pela tag: viagem.favorecido.documento.tipo
        /// 1 - CNPJ
        /// 2 - CPF
        /// 5 e 6 - RNTRC
        /// ### ATS
        /// - Caso CNPJ
        ///   - Consultar na tabela de proprietário se existe.
        /// - Para CPF
        ///   - Retornar OK somente se existir em ambas as tabela de proprietário e motorista. 
        /// </remarks>
        /// <returns></returns>
        [HttpPost]
        [Route("FindFleet")]
        public string FindFleet()
        {
            var frotaResponse = ConsultarFrotaTransportador(RequestXml);

            var frotaResult = ApiExecutor.ExecuteMapper("MAP_12fca58b-e140-4c83-a780-574c77135584",
                frotaResponse, true);
            
            return frotaResult.TransformedDocument;
        }

        private static string CriarXmlResposta(string codigo, string mensagem)
        {
            XNamespace env = "http://schemas.xmlsoap.org/soap/envelope/";
            XNamespace ns1 = "http://webservice.pamcard.jee.pamcary.com.br";

            var documentoResposta = new XDocument(
                new XElement(env + "Envelope", new
                        XAttribute(XNamespace.Xmlns + "env", "http://schemas.xmlsoap.org/soap/envelope/"),
                    new XElement(env + "Header"),
                    new XElement(env + "Body",
                        new XElement(ns1 + "executeResponse",
                            new XAttribute(XNamespace.Xmlns + "ns1",
                                "http://webservice.pamcard.jee.pamcary.com.br"),
                            new XElement("return",
                                new XElement("fields",
                                    new XElement("key", "mensagem.codigo"), new XElement("value", codigo)),
                                new XElement("fields",
                                    new XElement("key", "mensagem.descricao"), new XElement("value", mensagem))))
                    ))
            );            
            return documentoResposta.ToString();
        }

        private MapperResultBasicVO ConsultarMotorista(string cnpjEmpresa, string cpf)
        {
            var atsResult = AtsRepository.ConsultarMotorista(cnpjEmpresa, cpf);

            var parced = ApiExecutor.ExecuteParser("LAY_a20b0d0a-89f0-4fae-8462-4c721d4940f7", atsResult);
            var existMotorista = Convert.ToBoolean(ApiExecutor.GetValuesByElementXPath(parced.ParsedLayout.Elements, "Sucesso").FirstOrDefault()?.Value?.ToString());

            if (!existMotorista)
                return null;

            var mapperResultBasic = ApiExecutor.ExecuteMapper("MAP_b3eb73f7-e358-4171-8bc0-0e0dea75b084", atsResult, true);
            return mapperResultBasic;
        }

        private string ConsultarProprietario(string cnpjEmpresa, string cpf)
        {
            var atsResult = AtsRepository.ConsultarProprietario(cnpjEmpresa, cpf);

            var parced = ApiExecutor.ExecuteParser("LAY_fbfc399f-1333-452c-851d-e79ab9c748ec", atsResult);
            var existProprietario = Convert.ToBoolean(ApiExecutor
                .GetValuesByElementXPath(parced.ParsedLayout.Elements, "Sucesso").FirstOrDefault()?.Value?.ToString());

            if (!existProprietario)
                return CriarXmlResposta("4", parced.GetValueStringByElementXPath(ApiExecutor, "Mensagem"));
            
            var mapperResultBasic =
                ApiExecutor.ExecuteMapper("MAP_780ddbec-338e-4bc7-a21c-26ce93f35006", atsResult, true);
            return mapperResultBasic.TransformedDocument;
        }
        
        private string ConsultarRntrc(string cnpjEmpresa, string rntrc)
        {
            var atsResult = AtsRepository.ConsultarRntrc(cnpjEmpresa, rntrc);

            var mapperResultBasic =
                ApiExecutor.ExecuteMapper("MAP_fc69d5b2-4386-4520-86e4-49d54e99b339", atsResult, true);
            return mapperResultBasic.TransformedDocument;
        }
        
        private string IntegrarProprietario(string xml, string pamcardIndex)
        {
            var mapperResultBasic = ApiExecutor.ExecuteMapper(
                "MAP_5a373bcf-7d6a-447f-89bd-42551295e85a", xml,
                new Dictionary<string, object> {{"favorecidoIndex", pamcardIndex}},
                true);
            CheckMapperResultSucess(mapperResultBasic, "Proprietário request");
            
            if (mapperResultBasic.TransformedDocument.Contains("NaoReintegrarFavorecidos"))
                return "{\"Sucesso\": true}";
            
            var response = AtsRepository.IntegrarProprietario(mapperResultBasic.TransformedDocument);
            return response;
        }
        
        private string ConsultarFrotaTransportador(string xml)
        {
            var mapperResultBasic =
                ApiExecutor.ExecuteMapper("MAP_530b2acf-661f-43bf-a36a-89429c50b999", xml, true);
            
            var response = AtsRepository.ConsultarFrota(mapperResultBasic.TransformedDocument);
            return response;
        }
        
        private string IntegrarMotorista(string xml, string pamcardIndex)
        {
            var mapperResultBasic = ApiExecutor.ExecuteMapper(
                "MAP_a14ac484-98e8-495b-931a-e0249df19cf8", xml, 
                new Dictionary<string, object> {{"favorecidoIndex", pamcardIndex}},
                true);
            CheckMapperResultSucess(mapperResultBasic, "Motorista request");
            
            if (mapperResultBasic.TransformedDocument.Contains("NaoReintegrarFavorecidos"))
                return "{\"Sucesso\": true}";
                
            var response = AtsRepository.IntegrarMotorista(mapperResultBasic.TransformedDocument);
            return response;
        }

        /// <summary>
        /// Inserir favorecido
        /// </summary>
        /// <remarks>
        /// ### Pamcard
        /// - Cadastrar novo favorecido (Seja proprietário ou motorista)
        /// ### ATS
        /// - Caso CNPJ
        ///   - Inserir/atualizar na tabela de proprietário e retornar OK;
        /// - Caso CPF:
        ///   - Inserir/atualizar tabela de motorista;
        ///   - Inserir/atualizar tabela de proprietário;
        ///   - Retornar OK se existir o CPF em ambas as tabelas;
        /// </remarks>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertFavored")]
        public InsertFavoredResult InsertFavored(string pamcardIndex)
        {
            var keyBase = $"viagem.favorecido{pamcardIndex}.";
            var qtdDocumentos = PamcardManager.GetFieldValueInt(keyBase + "documento.qtde");            

            if (qtdDocumentos > 0)
            {                
                // Buscar CPF, caso nao encontrar busca o CNPJ
                var documento = PamcardManager.GetFieldValueIf(keyBase + "documento{0}.numero",
                    keyBase + "documento{0}.tipo", 2, qtdDocumentos);
                
                if (documento.IsNullOrWhiteSpace())
                    documento = PamcardManager.GetFieldValueIf(keyBase + "documento{0}.numero",
                        keyBase + "documento{0}.tipo", 1, qtdDocumentos);

                if (documento.IsNullOrWhiteSpace())
                    return new InsertFavoredResult(
                        false, CriarXmlResposta("4", "Por favor, informe um documento do tipo CPF ou CNPJ."));
                
                // Buscar RNTRC
                var rntrc = PamcardManager.GetFieldValueIf(keyBase + "documento{0}.numero",
                    keyBase + "documento{0}.tipo", 5, qtdDocumentos);
                
                if (rntrc.IsNullOrWhiteSpace())
                    rntrc = PamcardManager.GetFieldValueIf(keyBase + "documento{0}.numero",
                        keyBase + "documento{0}.tipo", 6, qtdDocumentos);
                
                // Motorista
                if (documento.Length <= 11)
                {
                    var motoristaResponse = IntegrarMotorista(RequestXml, pamcardIndex);
                    var parcedMotoristaResponse = ApiExecutor.ExecuteParser("LAY_d62900c3-7733-4eca-83ba-3fcfdcdbb578", motoristaResponse);
                    var sucessoMotorista = parcedMotoristaResponse.GetValueBoolByElementXPath(ApiExecutor, "Sucesso");
                    if (!sucessoMotorista)
                    {
                        var result = ApiExecutor.ExecuteMapper(
                            "MAP_7c78d920-fb42-4910-a836-05c6c5fd1fda", motoristaResponse, true);
                        return new InsertFavoredResult(false, result.TransformedDocument);
                    }
                }

                if (rntrc.HasValue())
                {
                    // Proprietário
                    var proprietarioResponse = IntegrarProprietario(RequestXml, pamcardIndex);
                    var parcedProprietarioResponse =
                        ApiExecutor.ExecuteParser("LAY_c3a0ac74-1920-46ac-83cf-941e692f04a9", proprietarioResponse);
                    var sucessoproprietario =
                        parcedProprietarioResponse.GetValueBoolByElementXPath(ApiExecutor, "Sucesso");
                    if (!sucessoproprietario)
                    {
                        var result = ApiExecutor.ExecuteMapper("MAP_599b6530-8b2d-4886-8a64-f0a04bca6d98",
                            proprietarioResponse, true);
                        return new InsertFavoredResult(false, result.TransformedDocument);
                    }
                }


                return new InsertFavoredResult(true, CriarXmlResposta("0", "Operacao realizada com sucesso."));
            }

            return new InsertFavoredResult(
                false, CriarXmlResposta("4", "Por favor, informe um documento do tipo CPF ou CNPJ."));
        }

        /// <summary>
        /// Consulta RNTRC para saber se está OK
        /// </summary>
        /// <remarks>
        ///### Pamcard [Não tenho certeza]
        /// - Verificar se RNTRC e CNPJ estão OK para emitir CIOT.
        /// - Se receber a placa acredito que valide a placa também.
        ///### ATS
        /// - Ir no micro serviço de CIOT para realizar a mesma operação realizada pela Pamcard
        /// </remarks>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>        
        [HttpPost]
        [Route("FindRNTRC")]
        public string FindRNTRC()
        {
            var cnpjEmpresa = PamcardManager.GetFieldValue("viagem.contratante.documento.numero");
            var qtdDocumentos = PamcardManager.GetFieldValueInt("viagem.favorecido.documento.qtde");

            for (var i = 1; i <= qtdDocumentos; i++)
            {
                if (qtdDocumentos > 0)
                {
                    const string keyBase = "viagem.favorecido.";
                    const int tipoRntrcCnpj = 6;
                    const int tipoRntrcCpf = 5;

                    // Buscar CPF, caso nao encontrar busca o CNPJ
                    var rntrc = PamcardManager.GetFieldValueIf(keyBase + $"documento{i + 1}.numero",
                        keyBase + "documento{0}.tipo", tipoRntrcCpf, qtdDocumentos);

                    if (string.IsNullOrWhiteSpace(rntrc))
                    {
                        rntrc = PamcardManager.GetFieldValueIf(keyBase + $"documento{i + 1}.numero",
                            keyBase + "documento{0}.tipo", tipoRntrcCnpj, qtdDocumentos);
                    }

                    if (string.IsNullOrWhiteSpace(rntrc))
                        return CriarXmlResposta("4", "Nenhum RNTRC informado na requisição!");

                    var consultarRntrc = ConsultarRntrc(cnpjEmpresa, rntrc);

                    return consultarRntrc;
                }
            }

            return CriarXmlResposta("4", "Proprietario não encontrado");
        }    
        
        /// <summary>
        /// Integrar viagem.
        /// No XML da Pamcard também vem os dados cadastrais do cliente origem/destino e também são integrados no ATS.
        /// </summary>
        /// <returns></returns>
        private string InsertFreightContract()
        {            
            #region Integrar cliente origem e destino
            
            string IntegrarClienteInternal(string tipo)
            {                              
                var qtdDocumentos = PamcardManager.GetFieldValueInt("viagem.documento1.pessoafiscal.qtde");

                for (var i = 1; i <= qtdDocumentos; i++)
                {
                    if (qtdDocumentos > 0)
                    {
                        var documento = PamcardManager.GetFieldValue($"viagem.documento1.pessoafiscal{i}.documento.numero");

                        if (documento == null)
                            return CriarXmlResposta("4", $"PessoaFiscal{i}.DocumentoNumero não pode ser vazio");
                    }
                }

                var atsClienteRequest = ApiExecutor.ExecuteMapper(
                    MapeadoresAtsPamcard.IntegrarClienteRequestId,
                    RequestXml,
                    new Dictionary<string, object> {{"tipo", tipo}},
                    true);
                CheckMapperResultSucess(atsClienteRequest, $"Cliente {tipo} request");

                var atsClienteResponse = AtsRepository.IntegrarCliente(atsClienteRequest.TransformedDocument);
                var atsClienteParsed = ApiExecutor.ExecuteParser(LayoutAts.IntegrarClienteResponse, atsClienteResponse);

                var sucessoCliente = atsClienteParsed.GetValueBoolByElementXPath(ApiExecutor, "Sucesso");
                var mensagemCliente = atsClienteParsed.GetValueStringByElementXPath(ApiExecutor, "Mensagem");

                if (!sucessoCliente)
                    return CriarXmlResposta("4", mensagemCliente);
                
                return null;
            }

            var resultClient = IntegrarClienteInternal("origem");
            if (resultClient != null)
                return resultClient;
            
            resultClient = IntegrarClienteInternal("destino");
            if (resultClient != null)
                return resultClient;
            
            #endregion
            
            #region Integrar favorecidos (Proprietário / Motoristas)

            var qtdFavorecidos = PamcardManager.GetFieldValueInt("viagem.favorecido.qtde");
            for (var i = 1; i <= qtdFavorecidos; i++)
            {
                var favResult = InsertFavored(i.ToString());
                if (!favResult.Sucesso)
                    return favResult.ResultXml;
            }

            #endregion
            
            #region Integrar veículo
            
            var atsVeiculoRequest = ApiExecutor.ExecuteMapper(
                MapeadoresAtsPamcard.IntegrarVeiculoRequestId, RequestXml, true);
            CheckMapperResultSucess(atsVeiculoRequest, "Veículo request");

            var atsVeiculoResponse = AtsRepository.IntegrarVeiculo(atsVeiculoRequest.TransformedDocument);
            var atsVeiculoParsed = ApiExecutor.ExecuteParser(LayoutAts.IntegrarVeiculoResponse, atsVeiculoResponse);

            var sucesso = atsVeiculoParsed.GetValueBoolByElementXPath(ApiExecutor, "Sucesso");
            var mensagem = atsVeiculoParsed.GetValueStringByElementXPath(ApiExecutor, "Mensagem");

            if (!sucesso)
                return CriarXmlResposta("4", mensagem);
            
            #endregion

            #region Integrar viagem
            
             var datatermino = PamcardManager.GetFieldValue("viagem.data.termino");

             if (datatermino == null)
                 return CriarXmlResposta("4", "ViagemDataTermino não pode ser vazio");
            
            var atsViagemRequest = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.IntegrarViagemRequestId, RequestXml, 
                true);
            CheckMapperResultSucess(atsViagemRequest, "Viagem request");
            
            var atsViagemResponse = AtsRepository.IntegrarViagem(atsViagemRequest.TransformedDocument);
            
            var pamcardResponse = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.IntegrarViagemResponseId, 
                atsViagemResponse, true);
            CheckMapperResultSucess(pamcardResponse, "Viagem response");
            
            return pamcardResponse.TransformedDocument;
            
            #endregion
        }

        /// <summary>
        /// Cancelar viagem
        /// </summary>
        /// <returns></returns>
        private string CancelTrip()
        {
            var cnpjEmpresa = PamcardManager.GetFieldValue("viagem.contratante.documento.numero");
            var idViagem = PamcardManager.GetFieldValueInt64("viagem.id");

            var atsResponse = AtsRepository.CancelarViagem(cnpjEmpresa, idViagem);
            var pamcardResponse = ApiExecutor.ExecuteMapper(
                MapeadoresAtsPamcard.CancelarViagemResponseId, atsResponse, true);
            CheckMapperResultSucess(pamcardResponse, "Cancelar viagem response");
            
            return pamcardResponse.TransformedDocument;
        }

        /// <summary>
        /// Consular viagem
        /// </summary>
        /// <returns></returns>
        private string FindFreightContract()
        {
            var atsRequest = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.ConsultarViagemRequestId, RequestXml, true);
            CheckMapperResultSucess(atsRequest, "Consultar viagem request");

            var atsResponse = AtsRepository.ConsultarViagem(atsRequest.TransformedDocument);

            var pamcardResponse = ApiExecutor.ExecuteMapper(
                MapeadoresAtsPamcard.ConsultarViagemResponseId, atsResponse, true);
            CheckMapperResultSucess(pamcardResponse, "Consultar viagem response");

            return pamcardResponse.TransformedDocument;
        }

        /// <summary>
        /// CloseFreightContract - Conversão não implementada
        /// </summary>
        /// <returns></returns>
        private string CloseFreightContract()
        {
            // Ação sem função no ATS.
            // Serve para modificar o peso e valor bruo do frete antes que seja baixado o saldo.
            // se necessário essas informação, deverá ser criado uma rotina no ATS para recepcionar isto.
            // Talvez seja relacionado a algum processo de CIOT agregado da Pamcard
            return CriarXmlResposta("0", "Operação realizada com sucesso");
        }
        
        [HttpPost]
        [Route("PayParcel")]
        public string PayParcel()
        {
            var resultJson = JsonConvert.SerializeObject(IntegrarViagemBaixaEvento(RequestXml));
            var mapperResultBasic = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.BaixarEventoResponseId, resultJson,
                true);

            CheckMapperResultSucess(mapperResultBasic, "PayParcel response");
            
            return mapperResultBasic.TransformedDocument;
        }
        
        [HttpPost]
        [Route("UpdateParcelStatus")]
        public string UpdateParcelStatus()
        {
            var mapperResultBasic = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.AlterarStatusParcelaRequestId, RequestXml, true);
            
            if (mapperResultBasic.ProcessMapperResult == ProcessMapperResultEnum.Success)
            {
                var response = AtsRepository.UpdateParcelStatus(mapperResultBasic.TransformedDocument);
                
                mapperResultBasic = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.AlterarStatusParcelaResponseId, 
                    response, true);

                CheckMapperResultSucess(mapperResultBasic, "UpdateParcelStatus response");
            
                return mapperResultBasic.TransformedDocument;
            }
            else
                throw new Exception(string.Join("\r\n", mapperResultBasic.ResultMessage.GetTransformationResultMessages().Select(x => x.Message).ToList()));
        }
        
        private PayParcelResult IntegrarViagemBaixaEvento(string xml)
        {
            var qtdParcelas = PamcardManager.GetFieldValueInt("viagem.parcela.qtde");
            if(qtdParcelas <= 0)
                throw new Exception("A tag viagem.parcela.qtde é obrigatória e não foi preenchida");
            
            var payParcelResult = new PayParcelResult(){ QuantidadeEventos = qtdParcelas};

            for (int i = 1; i <= qtdParcelas; i++)
            {
                var contextoDeDados = new Dictionary<string, object> {{"eventoIndex", i}};
                var mapperResultBasic = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.BaixarEventoRequestId, xml, 
                    contextoDeDados, true);

                if (mapperResultBasic.ProcessMapperResult == ProcessMapperResultEnum.Success)
                {
                    var response = AtsRepository.BaixarEvento(mapperResultBasic.TransformedDocument);
                    payParcelResult.Eventos.Add(JsonConvert.DeserializeObject<PayParcelEvento>(response));
                }
                else
                {
                    payParcelResult.Eventos.Add(new PayParcelEvento()
                    {
                        Sucesso = false,
                        Mensagem = string.Join("\r\n", mapperResultBasic.ResultMessage.GetTransformationResultMessages().Select(x => x.Message).ToList())
                    });
                }
            }

            return payParcelResult;
        }
        
        [HttpPost]
        [Route("Router")]
        public string Router()
        {
            var mapperResultBasic = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.ConsultarCustoPedagioRotaRequestId, 
                RequestXml, true);
            
            if (mapperResultBasic.ProcessMapperResult == ProcessMapperResultEnum.Success)
            {
                var response = AtsRepository.ConsultarCustoPedagioRota(mapperResultBasic.TransformedDocument);
                
                mapperResultBasic = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.ConsultarCustoPedagioRotaResponseId, 
                    response, true);

                CheckMapperResultSucess(mapperResultBasic, "Router response");
            
                return mapperResultBasic.TransformedDocument;
            }
            else
                throw new Exception(string.Join("\r\n", mapperResultBasic.ResultMessage.GetTransformationResultMessages().Select(x => x.Message).ToList()));
        }
                
        public string UpdateValuesFreightContract()
        {
            var mapperResultBasic = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.UpdateValuesFreightContractRequestId, 
                RequestXml, true);
            
            if (mapperResultBasic.ProcessMapperResult == ProcessMapperResultEnum.Success)
            {
                var response = AtsRepository.UpdateValuesFreightContract(mapperResultBasic.TransformedDocument);
                
                mapperResultBasic = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.UpdateValuesFreightContractResponseId, 
                    response, true);

                CheckMapperResultSucess(mapperResultBasic, "AlterarValoresViagem response");
            
                return mapperResultBasic.TransformedDocument;
            }
            else
                throw new Exception(string.Join("\r\n", mapperResultBasic.ResultMessage.GetTransformationResultMessages().Select(x => x.Message).ToList()));
        }

        [HttpPost]
        [Route("InsertTrip")]
        public string InsertTrip()
        {
            #region Integrar veículo
            
            var atsVeiculoRequest = ApiExecutor.ExecuteMapper(
                MapeadoresAtsPamcard.IntegrarVeiculoFromInsertTripRequestId, RequestXml, true);
            CheckMapperResultSucess(atsVeiculoRequest, "Veículo request (Trip)");

            var atsVeiculoResponse = AtsRepository.IntegrarVeiculo(atsVeiculoRequest.TransformedDocument);
            var atsVeiculoParsed = ApiExecutor.ExecuteParser(LayoutAts.IntegrarVeiculoResponse, atsVeiculoResponse);

            var sucesso = atsVeiculoParsed.GetValueBoolByElementXPath(ApiExecutor, "Sucesso");
            var mensagem = atsVeiculoParsed.GetValueStringByElementXPath(ApiExecutor, "Mensagem");

            if (!sucesso)
                return CriarXmlResposta("4", mensagem);
            
            #endregion
            
            #region Integrar Viagem
            
            var atsViagemRequest = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.InsertTripRequestId, RequestXml, true);
            CheckMapperResultSucess(atsViagemRequest, "Viagem request (Trip)");
            
            var atsViagemResponse = AtsRepository.IntegrarViagem(atsViagemRequest.TransformedDocument);
            
            var pamcardResponse = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.InsertTripResponseId, atsViagemResponse, 
                true);
            CheckMapperResultSucess(pamcardResponse, "Viagem response (Trip)");
            
            return pamcardResponse.TransformedDocument;
            
            #endregion
        }
        
        [HttpPost]
        [Route("FindCard")]
        public string FindCard()
        {
            // Resposta fake. Método não faz nenhuma consulta
            XNamespace env = "http://schemas.xmlsoap.org/soap/envelope/";
            XNamespace ns1 = "http://webservice.pamcard.jee.pamcary.com.br";

            var documentoResposta = new XDocument(
                new XElement(env + "Envelope", new
                        XAttribute(XNamespace.Xmlns + "env", "http://schemas.xmlsoap.org/soap/envelope/"),
                    new XElement(env + "Header"),
                    new XElement(env + "Body",
                        new XElement(ns1 + "executeResponse",
                            new XAttribute(XNamespace.Xmlns + "ns1",
                                "http://webservice.pamcard.jee.pamcary.com.br"),
                            new XElement("return",
                                new XElement("fields",
                                    new XElement("key", "mensagem.codigo"), new XElement("value", "0")),
                                new XElement("fields",
                                    new XElement("key", "mensagem.descricao"), new XElement("value", "Operação realizada com sucesso")),
                                new XElement("fields",
                                    new XElement("key", "viagem.cartao.portador.documento.numero"), new XElement("value", "30766886000140")),
                                new XElement("fields",
                                    new XElement("key", "viagem.cartao.portador.documento.tipo"), new XElement("value", "1")),
                                new XElement("fields",
                                    new XElement("key", "viagem.cartao.portador.nome"), new XElement("value", "Portador PEF-ATS")),
                                new XElement("fields",
                                    new XElement("key", "viagem.cartao.status.descricao"), new XElement("value", "LIBERADO")),
                                new XElement("fields",
                                    new XElement("key", "viagem.cartao.status.id"), new XElement("value", "1")),
                                new XElement("fields",
                                    new XElement("key", "viagem.cartao.tipo"), new XElement("value", "4"))
                            ))
                    ))
            );            
            return documentoResposta.ToString();
        }

        public string FindTrip()
        {
            var idViagem = PamcardManager.GetFieldValueInt64("viagem.id");
            var idViagemCliente = PamcardManager.GetFieldValue("viagem.id.cliente");
      
            if (idViagemCliente == null && idViagem <= 0)
                return CriarXmlResposta("4", "ViagemId e/ou ViagemIdCliente não pode ser vazio");
            
            var mapperResultBasic = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.FindTripRequestId, RequestXml, true);
            
            if (mapperResultBasic.ProcessMapperResult == ProcessMapperResultEnum.Success)
            {
                var response = AtsRepository.ConsultarViagem(mapperResultBasic.TransformedDocument);
                
                mapperResultBasic = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.FindTripResponseId, response, true);

                CheckMapperResultSucess(mapperResultBasic, "FindTrip response");
            
                return mapperResultBasic.TransformedDocument;
            }
            else
                throw new Exception(string.Join("\r\n", mapperResultBasic.ResultMessage.GetTransformationResultMessages().Select(x => x.Message).ToList()));
        }

        public string InsertParcel()
        {
            var mapperResultBasic = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.InserirParcelaRequestId, RequestXml, true);
            
            if (mapperResultBasic.ProcessMapperResult == ProcessMapperResultEnum.Success)
            {
                var response = AtsRepository.InserirParcela(mapperResultBasic.TransformedDocument);

                mapperResultBasic = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.InserirParcelaResponseId, response, true);

                CheckMapperResultSucess(mapperResultBasic, "InsertParcel response");
            
                return mapperResultBasic.TransformedDocument;
            }
            else
                throw new Exception(string.Join("\r\n", mapperResultBasic.ResultMessage.GetTransformationResultMessages().Select(x => x.Message).ToList()));
        }
        
        public string FindParcelStatus()
        {
            var idViagem = PamcardManager.GetFieldValueInt64("viagem.id");
            var idViagemCliente = PamcardManager.GetFieldValue("viagem.id.cliente");
            var numeroControle = PamcardManager.GetFieldValue("viagem.parcela.numero.cliente");
      
            if (idViagemCliente == null && idViagem <= 0)
                return CriarXmlResposta("4", "ViagemId e/ou ViagemIdCliente não pode ser vazio");            
            
            if (numeroControle == null)
                return CriarXmlResposta("4", "NumeroControle não pode ser vazio");
            
            var atsRequest = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.FindParcelStatusRequestId, RequestXml, true);
            CheckMapperResultSucess(atsRequest, "FindParcelStatus request");
            
            var atsResponse = AtsRepository.FindParcelStatus(atsRequest.TransformedDocument);
            
            var pamcardResponse = ApiExecutor.ExecuteMapper(
                MapeadoresAtsPamcard.FindParcelStatusResponseId, atsResponse, true);                        
            CheckMapperResultSucess(pamcardResponse, "FindParcelStatus response");

            var atsCartaoParsed = ApiExecutor.ExecuteParser(LayoutAts.FindParcelResponse, atsResponse);

            var sucesso = atsCartaoParsed.GetValueBoolByElementXPath(ApiExecutor, "Sucesso");
            var mensagem = atsCartaoParsed.GetValueStringByElementXPath(ApiExecutor, "Mensagem");
            
            if (!sucesso)
                return CriarXmlResposta("4", mensagem);                                             
            
            return pamcardResponse.TransformedDocument;                       
        }

        public string InsertCardFreight()
        {
            #region Integrar favorecidos (Proprietário / Motoristas)

            var keyBase = $"viagem.cartao.portador.";
            
            var documento = PamcardManager.GetFieldValueIf(keyBase + "documento.numero",
                keyBase + "documento.tipo", 2, 4);
                
            if (documento.IsNullOrWhiteSpace())
                documento = PamcardManager.GetFieldValueIf(keyBase + "documento.numero",
                    keyBase + "documento.tipo", 1, 4);                                                        
            
            if (documento.IsNullOrWhiteSpace())
                return CriarXmlResposta("4", "Tipo de Documento não inclui motorista");
            
            if (documento.Length <= 11)                
            {
                var atsMotoristaRequest = ApiExecutor.ExecuteMapper(
                    MapeadoresAtsPamcard.IntegrarMotoristaInsertCardRequestId, RequestXml, true);
                CheckMapperResultSucess(atsMotoristaRequest, "Motorista request (Card)");

                var atsMotoristaResponse = AtsRepository.IntegrarMotorista(atsMotoristaRequest.TransformedDocument);
                var atsMotoristaParsed = ApiExecutor.ExecuteParser(LayoutAts.MotoristaIntegrarResponse, atsMotoristaResponse);

                var sucesso = atsMotoristaParsed.GetValueBoolByElementXPath(ApiExecutor, "Sucesso");
                var mensagem = atsMotoristaParsed.GetValueStringByElementXPath(ApiExecutor, "Mensagem");

                if (!sucesso)
                    return CriarXmlResposta("4", mensagem);
            } 
            
            
            var atsProprietarioRequest = ApiExecutor.ExecuteMapper(
                MapeadoresAtsPamcard.IntegrarProprietarioInsertCardRequestId, RequestXml, true);
            CheckMapperResultSucess(atsProprietarioRequest, "Proprietario request (Card)");

            var atsProprietarioResponse = AtsRepository.IntegrarProprietario(atsProprietarioRequest.TransformedDocument);
            var atsProprietarioParsed = ApiExecutor.ExecuteParser(LayoutAts.ProprietarioIntegrarResponse, atsProprietarioResponse);

            var sucessoPro = atsProprietarioParsed.GetValueBoolByElementXPath(ApiExecutor, "Sucesso");
            var mensagemPro = atsProprietarioParsed.GetValueStringByElementXPath(ApiExecutor, "Mensagem");

            if (!sucessoPro)
                return CriarXmlResposta("4", mensagemPro);

            #endregion            
            
            #region Integrar Cartao
            
            var atsCartaoRequest = ApiExecutor.ExecuteMapper(MapeadoresAtsPamcard.InsertCardRequestId, RequestXml, true);
            CheckMapperResultSucess(atsCartaoRequest, "InsertCard request");
            
            var atsCartaoResponse = AtsRepository.InsertCard(atsCartaoRequest.TransformedDocument);
            
            var pamcardCartaoResponse = ApiExecutor.ExecuteMapper(
                MapeadoresAtsPamcard.InsertCardResponseId, atsCartaoResponse, true);
            CheckMapperResultSucess(pamcardCartaoResponse, "InsertCard response");
            
            var atsCartaoParsed = ApiExecutor.ExecuteParser(LayoutAts.IntegrarCartaoResponse, atsCartaoResponse);

            var sucessoCartao = atsCartaoParsed.GetValueBoolByElementXPath(ApiExecutor, "Sucesso");
            var mensagemCartao = atsCartaoParsed.GetValueStringByElementXPath(ApiExecutor, "Mensagem");
            
            if (!sucessoCartao)
                return CriarXmlResposta("4", mensagemCartao);            
            
            return pamcardCartaoResponse.TransformedDocument;   
            
            #endregion                                   
        }
        
        #region Métodos auxiliares

        private void CheckMapperResultSucess(MapperResultBasicVO mapperResult, string contextInfo)
        {                       
            if (mapperResult.ProcessMapperResult == ProcessMapperResultEnum.ErrorMapping ||
                mapperResult.ProcessMapperResult == ProcessMapperResultEnum.ErrorParserInput)
            {
                if (string.IsNullOrWhiteSpace(mapperResult.TransformedDocument))
                {
                    var msg = string.Join("\n", mapperResult.ResultMessage.GetTransformationResultMessages()
                        .Select(r => r.Message));
                    throw new Exception($"Falha ao converter layout - {contextInfo}: " + msg);
                }
            }
        }
        
        #endregion
    }
}