using System;
using System.Configuration;
using System.Data.SqlClient;

namespace SistemaInfo.ConnectUs.Api.Application.Util
{
    public static class AtsDb
    {
        public static object SqlSimple(string sql, params SqlParameter[] parameters)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ats"].ConnectionString))
            using (var cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = sql;

                if (parameters != null)
                    cmd.Parameters.AddRange(parameters);

                object result;
                try
                {
                    result = cmd.ExecuteScalar();
                }
                catch (Exception e)
                {
                    throw new Exception("Falha ao consultar DB ATS", e);
                }
                
                return result;
            }  
        }               
    }
}