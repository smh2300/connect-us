﻿using System.Configuration;
using SistemaInfo.Framework.Utils;

namespace SistemaInfo.ConnectUs.Api.Application.SistemaInfoServices
{
    public static class SistemaInfoConsts
    {
//        public static readonly string TokenAdministradora = ConfigurationManager.AppSettings["MS_TOKEN_ADMINISTRADORA"];

        private static readonly string BaseUrl = ConfigurationManager.AppSettings["MS.Url"].SetEndChars("/");       
        public static readonly string InfraApiUrl = BaseUrl + "Infra/Api";
        
        #region Identificadores do log gerado para a requisição atual

        /// <summary>
        /// Chave de identificação do id do log gerado para a requisição atual (HttpContext.Current.Items["key"])
        /// </summary>
        public const string CurrentLogIdSessionKey = "sie-bus-log-current-id";

        /// <summary>
        /// Chave de identificação do nível atual do log gerado para a requisição atual (HttpContext.Current.Items["key"])
        /// </summary>
        public const string CurrentLogNivelSessionKey = "sie-bus-log-current-nivel";

        #endregion

        #region Transmitir log atual para outro serviço da Sistema Info vincular seu processo a este

        public const string RestParentLogIdHeader = "bus-log-parent-id";
        public const string RestRootLogIdHeader = "bus-log-root-id";
        public const string RestCurrentLogNivelHeader = "bus-log-current-nivel";

        #endregion
    }
}
