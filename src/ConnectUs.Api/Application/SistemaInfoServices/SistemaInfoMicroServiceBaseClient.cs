﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using SistemaInfo.ConnectUs.Api.Application.SistemaInfoServices.Infra;
using SistemaInfo.Framework.Utils;
using SistemaInfo.MicroServices.Rest.Infra.ApiClient;

namespace SistemaInfo.ConnectUs.Api.Application.SistemaInfoServices
{
    public struct SistemaInfoMicroServiceClientParams
    {
        public RequestLogUtils.RequestLogInfo LogParams { get; set; }
        
        // Conteúdo interno manipulado pela classe SistemaInfoMicroServiceBaseClient
        internal Guid? OutputRequestId { get; set; }
    }
    
    public class SistemaInfoMicroServiceBaseClient
    {
        private SistemaInfoMicroServiceClientParams _params;
        
        public RequestLogUtils.RequestLogInfo CurrentLogParams
        {
            get => _params.LogParams;
            set => _params.LogParams = value;
        }
        
        public SistemaInfoMicroServiceBaseClient(HttpContext httpContext)
        {
            _params = new SistemaInfoMicroServiceClientParams
            {
                LogParams = RequestLogUtils.GetLogInfo(httpContext, default(Guid), 0, null)
            };            
        }
        
        public SistemaInfoMicroServiceBaseClient(SistemaInfoMicroServiceClientParams @params)
        {
            _params = @params;
        }
        
        protected async Task<HttpClient> CreateHttpClientAsync(CancellationToken cancellationToken)
        {
            var client = new HttpClient();
            return client;
        }

        protected async Task<HttpRequestMessage> CreateHttpRequestMessageAsync(CancellationToken cancellationToken)
        {
            var request = new HttpRequestMessage();
            ConfigureParentLogHeader(request);
            return request;
        }

        private void ConfigureParentLogHeader(HttpRequestMessage request)
        {
            // Quando é o próprio client do log executando a rotina, não deve entrar aqui
            if (this is Client)
                return;

            // Gravar log da requisição de saída
            if (_params.LogParams?.CurrentLogId != null)
            {
                var logId = _params.LogParams.CurrentLogId.Value;
                var logNivel = _params.LogParams.CurrentLogNivel;                   
    
                request.Headers.TryAddWithoutValidation(SistemaInfoConsts.RestParentLogIdHeader, logId.ToString());
                request.Headers.TryAddWithoutValidation(SistemaInfoConsts.RestRootLogIdHeader, logId.ToString());
                request.Headers.TryAddWithoutValidation(SistemaInfoConsts.RestCurrentLogNivelHeader, logNivel.ToString());
            }
        }

        #region Log para requisições de saída do ATS
        
        private LogIntegracaoInsertApiRequest LogPublishRequest(object request,
            string endpoint = null, string baseMethod = null, string metodo = null, string queryString = null, string verbo = null,
            string contentType = null, string headers = null)
        {
            if (_params.LogParams == null)
                return null;
                
            using (var infraRep = new InfraExternalRepository())
            {
                var logInfo = _params.LogParams;
                var log = infraRep.LogPublishRequest(                    
                    request,
                    _params.LogParams,
                    logInfo.CurrentLogId, logInfo.RootLogId ?? logInfo.CurrentLogId,
                    logInfo.CurrentLogNivel,
                    endpoint, baseMethod,
                    metodo, queryString, verbo, contentType, headers);

                return log;
            }
        }
        
        private void LogPublishResponse(Guid requestId, object response, int? statusCode, string headers = null)
        {
            using (var infraRep = new InfraExternalRepository())
            {
                infraRep.LogPublishResponse(_params.LogParams, requestId, response, statusCode, headers);
            }
        }

        
        
        public LogIntegracaoInsertApiRequest LogPublishRequest(HttpClient client, HttpRequestMessage request, string url, string baseUrl)
        {
            if (!InfraExternalRepository.EnableLogService || _params.LogParams == null)
                return null;

            // Log de saída a partir das partial classes do swagger
            var log = LogPublishRequest(
                GetContent(client, request, url),
                GetEndpoint(client, request, url),
                GetBaseMethod(client, request, url, baseUrl),
                GetMethodName(client, request, url, baseUrl),
                GetQueryString(client, request),
                request.Method.Method,
                request.Content?.Headers?.ContentType?.MediaType ?? request.Headers.Accept?.FirstOrDefault()?.MediaType,
                request.Headers.ToString());

            _params.OutputRequestId = log.Id.Value;
            
            // Sobreescrever informações do log atual ao header da requisição, para o que serviço que receberá este comando possa vincular ao parent log id gerado pelo ATS            
            if (request.Headers.Contains(SistemaInfoConsts.RestParentLogIdHeader))
                request.Headers.Remove(SistemaInfoConsts.RestParentLogIdHeader);
            
            request.Headers.TryAddWithoutValidation(
                SistemaInfoConsts.RestParentLogIdHeader, log.Id.Value.ToString());

            //
            return log;
        }
        
        public void LogPublishResponse(HttpClient client, HttpResponseMessage response)
        {
            if (!InfraExternalRepository.EnableLogService || !_params.OutputRequestId.HasValue || _params.LogParams == null) 
                return;
            
            LogPublishResponse(
                _params.OutputRequestId.Value,
                GetContent(response.Content),
                (int) response.StatusCode,
                response.Headers?.ToString());
        }
        
        #endregion

        //
        protected string GetLocalIpAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }

            return ".";
        }

        protected string GetEndpoint(HttpClient client, HttpRequestMessage request, string url)
        {
            const string template = "{0}://{1}";
            return template.FormatEx(request.RequestUri.Scheme, request.RequestUri.Authority);
        }

        protected string GetBaseMethod(HttpClient client, HttpRequestMessage request, string url, string baseMethod)
        {
            var endpoint = GetEndpoint(client, request, url);
            baseMethod = baseMethod.Remove(0, endpoint.Length);
            return baseMethod;
        }

        protected string GetMethodName(HttpClient client, HttpRequestMessage request, string url, string baseUrl)
        {
            var baseMethod = GetBaseMethod(client, request, url, baseUrl);

            // Removendo parte final da query string
            var index = url.LastIndexOf("?");
            if (index > -1)
                url = url.Substring(0, index);
            
            // Remover começo da URL
            index = url.LastIndexOf(baseMethod, StringComparison.InvariantCulture);
            if (index > -1)
            {
                var part = url.Substring(index + baseMethod.Length, url.Length - index - baseMethod.Length);
                return part;
            }
            return url;
        }

        protected string GetQueryString(HttpClient client, HttpRequestMessage request)
        {
            return request.RequestUri.Query;
        }

        protected object GetContent(HttpClient client, HttpRequestMessage request, string url)
        {
            var content = request.Content?.ReadAsStringAsync().Result;
            return content;
        }

        protected object GetContent(HttpContent content)
        {
            var ret = content?.ReadAsStringAsync().Result;
            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="field"></param>
        /// <param name="operador">Verificar CustomFilterOperator dos mapeamentos Swager</param>
        /// <param name="value"></param>
        /// <param name="fieldType">Verificar CustomFilterFieldType dos mapeamentos Swagger</param>
        protected string GetCustomFilterQueryString(string field, int @operador, string value, int fieldType)
        {
            // field~operator~valor~dataType(Optional)
            const string template = "{0}~{1}~{2}{3}";

            var op = string.Empty;
            var fType = string.Empty;
            switch (operador)
            {
                // StartsWith
                case 0:
                    op = "sw";
                    break;

                // EndsWith
                case 1:
                    op = "ew";
                    break;
                
                // Contains
                case 2:
                    op = "ct";
                    break;

                // NotContains
                case 3:
                    op = "nct";
                    break;                

                // Equals
                case 4:
                    op = "eq";
                    break;

                // NotEquals
                case 5:
                    op = "neq";
                    break;
                
                // IsNull
                case 6:
                    op = "null";
                    break;

                // IsNotNull
                case 7:
                    op = "notnull";
                    break;

                // GreaterThan
                case 8:
                    op = "gt";
                    break;

                // GreaterThanOrEqual
                case 9:
                    op = "gte";
                    break;

                // LessThan
                case 10:
                    op = "lt";
                    break;

                // LessThanOrEqual
                case 11:
                    op = "lte";
                    break;
                default: return string.Empty;
            }

            switch (fieldType)
            {
                case 0: break;
                case 1:
                    fType = "date";
                    break;
                case 2:
                    fType = "string";
                    break;
                case 3:
                    fType = "number";
                    break;
                case 4:
                    fType = "bool";
                    break;
                case 5:
                    fType = "intervalo";
                    break;
                default: break;
            }

            return template.FormatEx(
                field,
                op,
                value,
                string.IsNullOrWhiteSpace(fType) ? string.Empty : "~" + fType);
        }
    }
}
