﻿using System;
using System.Web;

namespace SistemaInfo.ConnectUs.Api.Application.SistemaInfoServices
{
    public class RequestLogUtils
    {
        public class RequestLogInfo
        {
            public Guid? CurrentLogId { get; set; }
            public int CurrentLogNivel { get; set; }
            //public Guid? ParentLogId { get; set; }
            public Guid? RootLogId { get; set; }
            public string TokenMicroservices { get; set; }
        }

        /// <summary>
        /// Obter metadados da requisição em execução para persitência na hierarquia de log's
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public static RequestLogInfo GetLogInfo(HttpContext httpContext, Guid currentLogId, int currentLogNivel, string tokenMicroservices)
        {
            Guid currrentLogId;
            int currentNivel = 0;

//            if (!httpContext.Items.Contains(SistemaInfoConsts.CurrentLogIdSessionKey))
//                return null;

            if (!httpContext.Items.Contains(SistemaInfoConsts.CurrentLogIdSessionKey))
            {
                if (HttpContext.Current?.Items != null && currentLogId != default(Guid))
                {
                    HttpContext.Current.Items.Add(SistemaInfoConsts.CurrentLogIdSessionKey, currentLogId);
                    HttpContext.Current.Items.Add(SistemaInfoConsts.CurrentLogNivelSessionKey, currentLogNivel);
                }
            }

            var currentLogIdObj = httpContext.Items[SistemaInfoConsts.CurrentLogIdSessionKey];
            if (!Guid.TryParse(currentLogIdObj.ToString(), out currrentLogId))
                return null;

            if (httpContext.Items.Contains(SistemaInfoConsts.CurrentLogIdSessionKey))
            {
                var nivelObj = httpContext.Items[SistemaInfoConsts.CurrentLogNivelSessionKey];
                int.TryParse(nivelObj.ToString(), out currentNivel);
            }

            return new RequestLogInfo
            {
                CurrentLogId = currrentLogId,
                CurrentLogNivel = currentNivel,
                RootLogId = currrentLogId,
                TokenMicroservices = tokenMicroservices
            };
        }
    }
}