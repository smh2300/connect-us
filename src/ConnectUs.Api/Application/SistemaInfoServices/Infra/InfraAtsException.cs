﻿using System;
using System.Runtime.Serialization;

namespace SistemaInfo.ConnectUs.Api.Application.SistemaInfoServices.Infra
{
    public class InfraAtsException : Exception
    {
        public InfraAtsException()
        {
        }

        public InfraAtsException(string message) : base(message)
        {
        }

        public InfraAtsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InfraAtsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
