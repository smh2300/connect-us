﻿using System;
using System.Collections.Concurrent;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using NLog;
using SistemaInfo.ConnectUs.Api.Application.SistemaInfoServices.Infra.Log;
using SistemaInfo.Framework.Utils;
using SistemaInfo.MicroServices.Rest.Infra.ApiClient;

namespace SistemaInfo.ConnectUs.Api.Application.SistemaInfoServices.Infra
{
    public class InfraExternalRepository : IDisposable
    {
        private struct LogParams
        {
            public object Data { get; set; }
            public RequestLogUtils.RequestLogInfo RequestInfo { get; set; }
            public string AuditUserDoc { get; set; }
            public string AuditUserName { get; set; }            
        }
        
        private const string ServicoIndisponivelResultMessage = "Serviço de infraestrutura indisponível";

        private static readonly string MicroServiceName;
        private static string ProductTitle;
        public static bool EnableLogService { get; set; } = true;
        public const string AuditDocIntegracao =  "00000000000";
        // HttpContext.Current.Items["CpfUsuarioLogado"]
               
        private static readonly BlockingCollection<LogParams> LogsQueue = new BlockingCollection<LogParams>();
        
        static InfraExternalRepository()
        {
            MicroServiceName = ConfigurationManager.AppSettings["MS.Logs.NomeServico"] ?? "ConnectUs";
            ProductTitle = "ATS";
            
            if (ConfigurationManager.AppSettings["MS.Url"].HasValue())
            {
                var config = ConfigurationManager.AppSettings["MS.Logs.EnableApiLogs"];
                if (config.HasValue())
                {
                    if (bool.TryParse(config, out var enable))
                        EnableLogService = enable;
                }
            }
            else
                EnableLogService = false;

            if (EnableLogService)
                Task.Factory.StartNew(ProcessarLogsQueue);
        }

        public static void Initialize(string productTitle)
        {
            ProductTitle = productTitle;            
        }

        public InfraExternalRepository()
        {
        }

        public void Dispose()
        {
        }
        
        #region Monitoramento assincrono da fila de log's para armazenamento no micro serviço

        private static void ProcessarLogsQueue()
        {
            var apiClient = new global::SistemaInfo.MicroServices.Rest.Infra.ApiClient.Client(new SistemaInfoMicroServiceClientParams());
            apiClient.BaseUrl = SistemaInfoConsts.InfraApiUrl;
            
            while (true)
            {
                var log = default(LogParams);
                try
                {
                    // Este tipo de coleção bloqueia o a execução na linha do Take caso não haver nenhum item na lista, e quando um item for adicionado, e execução é liberada.                    
                    log = LogsQueue.Take();
                    
                    switch (log.Data)
                    {
                        case null:
                            continue;
                                                        
                        case LogIntegracaoInsertApiRequest requestData:
                            // Integração de requisição
                            try
                            {
                                apiClient.LogIntegracoesPost(requestData,
                                    log.RequestInfo.TokenMicroservices, log.AuditUserDoc, log.AuditUserName);
                            }
                            catch (Exception e)
                            {
                                LogsQueue.Add(log);
//                                LogManager.GetCurrentClassLogger()
//                                    .Error(e, "Erro ao integrar log de requisição no microserviço. Log Params: " + SerializeToSaveInLogSafe(log.Data));
                            }

                            break;
                                                        
                        case LogIntegracaoUpdateApiRequest responseData:
                            // Integração de resposta
                            try
                            {
                                apiClient.LogIntegracoesPut(responseData,
                                    log.RequestInfo.TokenMicroservices, log.AuditUserDoc, log.AuditUserName);                                
                            }
                            catch (Exception e)
                            {
                                LogsQueue.Add(log);
//                                LogManager.GetCurrentClassLogger()
//                                    .Error(e, "Erro ao integrar log de resposta no microserviço. Log Params: " + SerializeToSaveInLogSafe(log.Data));                                
                            }

                            break;
                            
                        default:
                            // Algo inesperado
//                            LogManager.GetCurrentClassLogger().Info("Tipo de log inesperado para armazenamento: " + log.Data.GetType().FullName + "\n" + SerializeToSaveInLogSafe(log.Data));                            
                            break;
                    }

                    // Remove se conseguiu processadar o comando sem erros (Se haver erro de comunicaçao com o Micro Serviço, o item permanece na lista para armazenamento).
                    //LogsQueue.TryDequeue(out log);
                }
                catch (Exception e)
                {
                    LogManager.GetCurrentClassLogger()
                        .Error(e, "Erro ao integrar log de requisição/resposta no microserviço. Log Params: " + SerializeToSaveInLogSafe(log.Data));                    
                }
            }
        }

        public static LogProcessInfo GetProcessInfo()
        {                        
            var result = new LogProcessInfo
            {
                WaitingForSync = LogsQueue.Count                
            };

            return result;
        }
        
        #endregion

        #region Persistência de logs no micro serviço
        
        private void EnqueueRequest(LogIntegracaoInsertApiRequest request, RequestLogUtils.RequestLogInfo currentLogInfo)
        {
            try
            {
                if (request.Id == null || request.Id == default(Guid))
                    request.Id = Guid.NewGuid();

//#if DEBUG
//                LogManager.GetLogger("buslog").Info("Request->" + JsonConvert.SerializeObject(request));
//#endif                
                LogsQueue.Add(new LogParams
                {
                    Data = request,
                    RequestInfo = currentLogInfo, //RequestLogUtils.GetLogInfo(HttpContext.Current),
                    AuditUserDoc = AuditDocIntegracao,
                    AuditUserName = null
                });                
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger()
                    .Error(e, "Erro ao salvar log de requisição no micro serviço (integração). Request: " + request.ToJson());
                throw;
            }
        }

        private void EnqueueResponse(LogIntegracaoUpdateApiRequest response, RequestLogUtils.RequestLogInfo currentLogInfo)
        {
            try
            {
//#if DEBUG
//                LogManager.GetLogger("buslog").Info("Response->" + JsonConvert.SerializeObject(response));
//#endif                
                LogsQueue.Add(new LogParams
                {
                    Data = response,
                    RequestInfo = currentLogInfo, //RequestLogUtils.GetLogInfo(HttpContext.Current),
                    AuditUserDoc = AuditDocIntegracao,
                    AuditUserName = null
                });
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger()
                    .Error(e, "Erro ao salvar log de resposta no micro serviço (integração). Response: " + response.ToJson());
            }
        }

        #endregion
        
        #region Log de entrada no ATS (Request/Response)

        /// <summary>
        /// Salvar log das requisições que a API do ATS recebeu do consumidor
        /// </summary>
        /// <param name="filterContext"></param>
        /// <returns></returns>
        /*public LogIntegracaoInsertApiRequest LogConsumerRequest(HttpActionContext filterContext)
        {
            string bodyText = null;
            try
            {                
                var queryUrl = filterContext.HttpContext.Request.Url?.Query;
                if (queryUrl.HasValue())
                    bodyText += queryUrl + "\n";

                if (filterContext.HttpContext.Request.InputStream.Length > 0)
                    using (var reader = new StreamReader(filterContext.HttpContext.Request.InputStream))
                    {
                        reader.BaseStream.Seek(0, SeekOrigin.Begin);
                        bodyText += reader.ReadToEnd();
                    }

                bodyText = bodyText?.Trim();

                var log = new LogIntegracaoInsertApiRequest();
                log.MicroServico = MicroServiceName;
                log.Aplicacao = ProductTitle; // Assembly.GetCallingAssembly().GetName().Name;
                log.BasePath = filterContext.HttpContext.Request.ApplicationPath.RemoveStartValue("/").RemoveEndValue("/");
                log.Metodo = filterContext.HttpContext.Request.Path.RemoveStartValue("/").RemoveEndValue("/");
                log.Verbo = filterContext.HttpContext.Request.HttpMethod;
                log.ContentyType = filterContext.HttpContext.Request.ContentType;
                
                log.Direcao = LogIntegracaoInsertApiRequestDirecao.Input;
                log.HostNameServidor = Environment.MachineName;
                log.IpServidor = GetLocalIpAddress();
                log.IpRequisicao = filterContext.HttpContext.Request.UserHostAddress;

                log.DataRequisicao = DateTime.Now;
                log.Requisicao = bodyText;
                log.HeaderRequisicao = HeaderToString(filterContext.HttpContext?.Request?.Headers);

                //log.ParentId = engine.RequestSession.Get<Guid?>(LoggingConsts.ParentIdSessionKey);
                //log.RootId = engine.RequestSession.Get<Guid?>(LoggingConsts.RootIdSessionKey);
                log.Nivel = 0;

                EnqueueRequest(log, RequestLogUtils.GetLogInfo(HttpContext.Current));
                return log;
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger()
                    .Error(e, "Erro ao salvar log de requisição no micro serviço (informações). Body: " + bodyText);
            }

            return null;
        }*/

        /// <summary>
        /// Salvar log das resposta que a API do ATS retornou ao consumidor
        /// </summary>
        /// <param name="filterContext"></param>
        /// <param name="requestId"></param>
        /*public void LogConsumerResponse(ActionExecutedContext filterContext, Guid requestId)
        {
            if (requestId == default(Guid))
                return;

            string bodyText = null;
            try
            {
                if (filterContext.Result is JsonResult jsonResult)
                    bodyText = JsonConvert.SerializeObject(jsonResult.Data);
                else if (filterContext.Result is ContentResult contentResult)
                    bodyText = contentResult.Content;
                else if (filterContext.Result != null)
                    bodyText = "Formato de resultado inesperado para armazenar resposta. Result Type: " + filterContext.Result.GetType().FullName;

                var log = new LogIntegracaoUpdateApiRequest();
                log.Id = requestId;
                log.DataResposta = DateTime.Now;
                log.Resposta = bodyText;
                log.HeaderResposta = HeaderToString(filterContext.HttpContext?.Response?.Headers);
                log.StatusCodeResposta = filterContext.HttpContext?.Response?.StatusCode;

                EnqueueResponse(log, RequestLogUtils.GetLogInfo(HttpContext.Current));
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger()
                    .Error(e, "Erro ao salvar log de resposta no micro serviço (informações). Id: {0} - Body Response: {1}", requestId, bodyText);
            }
        }*/

        #endregion
        
        #region Log de entrada no serviço de conversão ATS x Pamcard
        
        public LogIntegracaoInsertApiRequest LogInputRequest(string tokenMicroService, string pamcardMethod, string requestXml)
        {
            var log = new LogIntegracaoInsertApiRequest();
            try
            {                
                log.Id = Guid.NewGuid();
                log.MicroServico = MicroServiceName;
                log.Aplicacao = ProductTitle; // Assembly.GetCallingAssembly().GetName().Name;
                log.BasePath = "PamcardToAts/Execute";
                log.Metodo = pamcardMethod;
                log.Verbo = "POST";
                log.ContentyType = "xml";
                
                log.Direcao = LogIntegracaoInsertApiRequestDirecao.Input;
                log.HostNameServidor = Environment.MachineName;
                log.IpServidor = GetLocalIpAddress();
//                log.IpRequisicao = filterContext.HttpContext.Request.UserHostAddress;

                log.DataRequisicao = DateTime.Now;
                log.Requisicao = requestXml;
//                log.HeaderRequisicao = HeaderToString(filterContext.HttpContext?.Request?.Headers);

                //log.ParentId = engine.RequestSession.Get<Guid?>(LoggingConsts.ParentIdSessionKey);
                //log.RootId = engine.RequestSession.Get<Guid?>(LoggingConsts.RootIdSessionKey);
                log.Nivel = 0;

                EnqueueRequest(log, RequestLogUtils.GetLogInfo(HttpContext.Current, 
                    log.Id.GetValueOrDefault(), log.Nivel.GetValueOrDefault(), tokenMicroService));
                return log;
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger()
                    .Error(e, "Falha ao enviar log de requisição ao micro serviço. Id: {0} - Requisição: {1}"
                        .FormatEx(log.Id, requestXml));
            }

            return null;
        }
        
        public void LogInputResponse(Guid logId, string tokenMicroService, string responseXml)
        {            
            try
            {
                if (logId == default(Guid))
                    return;                              

                var log = new LogIntegracaoUpdateApiRequest();
                log.Id = logId;
                log.DataResposta = DateTime.Now;
                log.Resposta = responseXml;
//                log.HeaderResposta = HeaderToString(filterContext.HttpContext?.Response?.Headers);
                log.StatusCodeResposta = 200;

                EnqueueResponse(log, RequestLogUtils.GetLogInfo(HttpContext.Current,
                    logId, 0, tokenMicroService));
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger()
                    .Error(e, "Falha ao enviar log de resposta ao micro serviço. Id: {0} - Resposta: {1}", logId, responseXml);
            }
        }
        
        #endregion

        #region Log de saídas do ATS para outros sistemas

        public LogIntegracaoInsertApiRequest LogPublishRequest(object request,
            RequestLogUtils.RequestLogInfo currentLogInfo,
            Guid? parentLogId, Guid? rootId, int nivel,
            string endpoint = null, string baseMethod = null, string metodo = null, string queryString = null, string verbo = null,
            string contentType = null, string headers = null)
        {
            string stringRequest = null;
            try
            {
                if (request != null)
                    stringRequest = request is string s ? s : JsonConvert.SerializeObject(request);

                var log = new LogIntegracaoInsertApiRequest();
                log.Id = Guid.NewGuid();
                log.MicroServico = MicroServiceName;
                log.Aplicacao = ProductTitle;
                log.BasePath = baseMethod.RemoveStartValue("/").RemoveEndValue("/");
                log.Metodo = metodo.RemoveStartValue("/").RemoveEndValue("/");
                log.QueryString = queryString.RemoveStartValue("?");
                log.Verbo = verbo;
                log.ContentyType = contentType;

                log.Direcao = LogIntegracaoInsertApiRequestDirecao.Output;
                log.HostNameServidor = Environment.MachineName;
                log.IpServidor = GetLocalIpAddress();
                log.IpRequisicao = endpoint;

                log.DataRequisicao = DateTime.Now;
                log.Requisicao = stringRequest;
                log.HeaderRequisicao = headers;

                log.ParentId = parentLogId;
                log.RootId = rootId;
                log.Nivel = nivel;

                EnqueueRequest(log, currentLogInfo);
                return log;
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger()
                    .Error(e, "Erro ao salvar log de requisição de saída no micro serviço (informações). Body: " + stringRequest);
                throw;
            }
        }

        public void LogPublishResponse(RequestLogUtils.RequestLogInfo currentLogInfo, Guid requestId, object response, int? statusCode, string headers = null)
        {
            if (requestId == default(Guid))
                return;

            string bodyText = null;
            try
            {
                if (response != null)
                    bodyText = response is string s ? s : JsonConvert.SerializeObject(response);

                var log = new LogIntegracaoUpdateApiRequest();
                log.Id = requestId;
                log.DataResposta = DateTime.Now;
                log.Resposta = bodyText;
                log.HeaderResposta = headers;
                log.StatusCodeResposta = statusCode;

                EnqueueResponse(log, currentLogInfo);
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger()
                    .Error(e,
                        "Erro ao salvar log de resposta de saída no micro serviço (informações). Id: {0} - Body Response: {1}",
                        requestId, bodyText);
            }
        }

        #endregion

        #region Funções auxiliares
        
        private string GetLocalIpAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }

            return ".";
        }

        private string HeaderToString(NameValueCollection requestHeaders)
        {
            if (requestHeaders == null)
                return null;

            var builder = new StringBuilder(Environment.NewLine);
            for (var i = 0; i < requestHeaders.Count; i++)
                builder.AppendLine($"{requestHeaders.GetKey(i)}:{requestHeaders.Get(i)}");
            return builder.ToString().Trim();
        }
        
        private static string SerializeToSaveInLogSafe(object data)
        {
            try
            {                        
                return JsonConvert.SerializeObject(data);
            }
            catch
            {
                return null;
            }
        }
        
        #endregion
    }
}
