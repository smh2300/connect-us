﻿using System.Net.Http;

// ReSharper disable once CheckNamespace
namespace SistemaInfo.MicroServices.Rest.Infra.ApiClient
{
    public partial class NotificacaoClient
    {
        partial void PrepareRequest(HttpClient client, HttpRequestMessage request, string url)
        {
            LogPublishRequest(client, request, url, BaseUrl);
        }

        partial void ProcessResponse(HttpClient client, HttpResponseMessage response)
        {
            LogPublishResponse(client, response);
        }
    }

}
