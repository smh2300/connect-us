using System;
using System.Collections.Generic;
using System.Linq;
using SysMiddle.ConnectUs.API.Service;
using SysMiddle.ConnectUs.Core.Interface.ValueObject.API;
using SysMiddle.ConnectUs.Core.Interface.ValueObject.GeneralObjects.Parent;

namespace SistemaInfo.ConnectUs.Api.Application.SysMiddle
{
    public static class ConnectUs
    {
        public const string ContextName = "Sistema Info";
        public const string ProjectId = "0f3fcd8a-871e-49b2-9841-e59da23750f7";                       

        private static APIExecutor _executor = null;

        public static APIExecutor ApiExecutor =>
            _executor ?? (_executor = APIManager.Instance.GetApiExecutorByIdentifier(ContextName, ProjectId));

        
        #region Extensões para objetos Connect Us
        
        // GetValueStringByElementXPath
        public static string GetValueStringByElementXPath(this IEnumerable<ElementVO> elements, APIExecutor executor, string elementXPath)
        {
            var data = ApiExecutor.GetValuesByElementXPath(elements, elementXPath);
            return data?.FirstOrDefault()?.Value?.ToString();
        }

        public static string GetValueStringByElementXPath(this ParserResultBasicVO parser, APIExecutor executor,
            string elementXPath)
        {
            return parser.ParsedLayout.Elements.GetValueStringByElementXPath(executor, elementXPath);
        }

        // GetValueBoolByElementXPath
        public static bool GetValueBoolByElementXPath(this IEnumerable<ElementVO> elements, APIExecutor executor,
            string elementXPath, bool? @default = false)
        {
            var resultStr = GetValueStringByElementXPath(elements, executor, elementXPath);
            
            if (string.IsNullOrWhiteSpace(resultStr) || 
                !bool.TryParse(resultStr, out var result))
            {
                if (@default == null)
                    throw new Exception("Valor boolean inválido para xpath: " + elementXPath);
                return @default.Value;
            }

            return result;
        }
        
        public static bool GetValueBoolByElementXPath(this ParserResultBasicVO parser, APIExecutor executor,
            string elementXPath)
        {
            return parser.ParsedLayout.Elements.GetValueBoolByElementXPath(executor, elementXPath);
        }
        
        
        #endregion
    }
}