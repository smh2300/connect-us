namespace SistemaInfo.ConnectUs.Api.Application.SysMiddle
{
    public static class LayoutPamcard
    {
        /// <summary>
        /// WsTransacional-Request
        /// </summary>
        public const string WsTransacionalRequest = "LAY_a709f916-7010-4b35-8cbd-4819034af131";
        
        /// <summary>
        /// WsTransacional-Response
        /// </summary>
        public const string WsTransacionalResponse = "LAY_3ba86490-adbf-4441-bf9b-ba77e87ac2d8";
    }
}