namespace SistemaInfo.ConnectUs.Api.Application.SysMiddle
{
    public static class MapeadoresAtsPamcard
    {
        /// <summary>
        /// Viagem/Integrar-Request-PEF
        /// </summary>
        public const string IntegrarViagemRequestId = "MAP_acca00d1-5dd3-48f7-8a18-85b81f10f986";
        
        /// <summary>
        /// Viagem/Integrar-Response-PEF
        /// </summary>
        public const string IntegrarViagemResponseId = "MAP_6795797a-f769-4d8c-b4f9-e9540de9666a";
        
        /// <summary>
        /// Cliente/Integrar-Request-PEF
        /// </summary>
        public const string IntegrarClienteRequestId = "MAP_1d1e852d-6f9f-47ce-b129-e00e3007d159";

        /// <summary>
        /// Veiculo/Integrar-PEF
        /// </summary>
        public const string IntegrarVeiculoRequestId = "MAP_d12d7671-8085-418c-a993-23e63f22995f";

        /// <summary>
        /// Viagem/CancelarViagem-Response-PEF
        /// </summary>
        public const string CancelarViagemResponseId = "MAP_21b8ef31-7758-4ac3-8f91-4df2413aae41";

        /// <summary>
        /// Viagem/Consultar-Request-PEF
        /// </summary>
        public const string ConsultarViagemRequestId = "MAP_bc281212-ca48-4e80-ab0b-0738c792e64d";
        
        /// <summary>
        /// Viagem/Consultar-Response-PEF
        /// </summary>
        public const string ConsultarViagemResponseId = "MAP_ecac4207-e3e1-43cd-93d2-3d5850d9ca4c";

        public const string BaixarEventoRequestId = "MAP_12081352-a6fe-4bd8-b707-2f72356aa443";
        
        public const string BaixarEventoResponseId = "MAP_d5677bcb-98e3-496f-b77d-dc76ad36957c";
        
        public const string ConsultarCustoPedagioRotaRequestId = "MAP_7fea3d19-9239-48db-8f9a-6156656d1f49";                
        
        public const string ConsultarCustoPedagioRotaResponseId = "MAP_7e117127-3d17-4c54-b2bd-cb72db030d39";

        public const string AlterarStatusParcelaRequestId = "MAP_804e0eb9-84e2-4728-b241-fec1778546d1";
        
        public const string AlterarStatusParcelaResponseId = "MAP_243f9eb3-86dc-40a7-a66f-aec69f3b4a35";

        public const string InserirParcelaRequestId = "MAP_df5cc71c-e32f-4381-b494-95b017be7110";
        
        public const string InserirParcelaResponseId = "MAP_3c5d60eb-a903-4c52-aa31-8bf58d5b4bcc";
        
        /// <summary>
        /// Viagem/AlterarValoresViagem-Request-PEF
        /// </summary>
        public const string UpdateValuesFreightContractRequestId = "MAP_79fdc443-5fe2-453b-a478-7f9ab2bbeb58";
        
        /// <summary>
        /// Viagem/AlterarValoresViagem-Response-PEF
        /// </summary>
        public const string UpdateValuesFreightContractResponseId = "MAP_ba588347-52d0-42ba-92d5-13220e38e4ad";

        /// <summary>
        /// Viagem/InsertTrip-Request-PEF
        /// </summary>
        public const string InsertTripRequestId = "MAP_df32b866-9167-4072-ae8f-3e47bdca9f7c";
        
        /// <summary>
        /// Viagem/InsertTrip-Response-PEF
        /// </summary>
        public const string InsertTripResponseId = "MAP_f0e2e364-f7d7-4b8b-8966-ba3fde859909";
        
        /// <summary>
        /// Veiculo/Integrar-Request-InsertTrip-PEF
        /// </summary>
        public const string IntegrarVeiculoFromInsertTripRequestId = "MAP_991af032-96f6-490a-9478-7e40ac4b82c1";

        /// <summary>
        /// Viagem/ConsultarStatusParcela-Request-PEF
        /// </summary>
        public const string FindParcelStatusRequestId = "MAP_edd0fe16-5bfe-4538-93a3-3c1bfabed450";
        
        /// <summary>
        /// Viagem/ConsultarStatusParcela-Reponse-PEF
        /// </summary>
        public const string FindParcelStatusResponseId = "MAP_1a36b700-7aa6-47c3-a643-2a61adcdd18b";
        
        /// <summary>
        /// Viagem/FindTrip=RequestPEFToAts
        /// </summary>
        public const string FindTripRequestId = "MAP_923813f4-dca2-43ff-9221-df58e768fa05";
        
        /// <summary>
        /// Viagem/FindTrip-Response-AtsToPEF
        /// </summary>
        public const string FindTripResponseId = "MAP_b8452b2d-16f9-4f9f-a0a8-361ad1375044";
        
        /// <summary>
        /// Motorista/Integrar-Request-InsertCard-PEF
        /// </summary>
        public const string IntegrarMotoristaInsertCardRequestId = "MAP_680188a2-e03a-4732-9e4f-63f8a0a97662";
        
        /// <summary>
        /// Proprietario/Integrar-Request-InsertCard-PEF
        /// </summary>
        public const string IntegrarProprietarioInsertCardRequestId = "MAP_d89e4002-a912-479a-a935-ee813ab44947";
        
        /// <summary>
        /// Viagem/IntegrarCartao-Request-PEF
        /// </summary>
        public const string InsertCardRequestId = "MAP_586e92ce-6532-4e08-b36d-2b760ecbf2f8";
        
        /// <summary>
        /// Viagem/IntegrarCartao-Response-PEF
        /// </summary>
        public const string InsertCardResponseId = "MAP_a4e27da0-58ce-47fc-af3a-5c7d4ea05ba7";
    }
}