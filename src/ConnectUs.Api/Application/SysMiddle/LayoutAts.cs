namespace SistemaInfo.ConnectUs.Api.Application.SysMiddle
{
    public static class LayoutAts
    {
        /// <summary>
        /// Cliente/Integrar-Response
        /// </summary>
        public const string IntegrarClienteResponse = "LAY_b734471d-ca98-4daf-8e92-8b0c745c2639";
        
        /// <summary>
        /// Veiculo/Integrar-Response
        /// </summary>
        public const string IntegrarVeiculoResponse = "LAY_da439ccc-0e1a-4d66-9396-df7ce3f0c047";
        
        /// <summary>
        /// Motorista/Integrar-Response
        /// </summary>
        public const string MotoristaIntegrarResponse = "LAY_d62900c3-7733-4eca-83ba-3fcfdcdbb578";
        
        /// <summary>
        /// Proprietario/Integrar-Response
        /// </summary>
        public const string ProprietarioIntegrarResponse = "LAY_c3a0ac74-1920-46ac-83cf-941e692f04a9";
        
        /// <summary>
        /// Viagem/IntegrarCartao-Response
        /// </summary>
        public const string IntegrarCartaoResponse = "LAY_a1f4dcdc-5233-4596-8177-9e5ffa77ae06";
        
        /// <summary>
        /// Viagem/ConsultarParcela-Response
        /// </summary>
        public const string FindParcelResponse = "LAY_fe25259e-c55b-4f83-b244-457d801dacf5";
    }
}