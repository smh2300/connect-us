namespace SistemaInfo.ConnectUs.Api.Application.Objects
{
    public class InsertFavoredResult
    {
        public InsertFavoredResult(bool sucesso, string resultXml)
        {
            Sucesso = sucesso;
            ResultXml = resultXml;
        }

        public bool Sucesso { get; set; }
        public string ResultXml { get; set; }
    }
}