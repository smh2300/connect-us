using System.Collections.Generic;

namespace SistemaInfo.ConnectUs.Api.Application.Objects
{
    public class PayParcelResult
    {
        public int QuantidadeEventos { get; set; }
        public List<PayParcelEvento> Eventos { get; set; } = new List<PayParcelEvento>();
    }

    public class PayParcelEvento
    {
        public bool Sucesso { get; set; }
        public string Mensagem { get; set; }
        public int IdViagem { get; set; }
        public string NumeroControle { get; set; }
        public int IdViagemEvento { get; set; }
        public string NumeroControleEvento { get; set; }
        public OperacaoCartao OperacaoCartao { get; set; }
        public string Token { get; set; }
    }

    public class OperacaoCartao
    {
        public int Status { get; set; }
        public string Mensagem { get; set; }
    }
}