﻿using System;

namespace SistemaInfo.ConnectUs.Api.Application.Filters
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class IgnoreRequestLog : Attribute
    {
    }
}