﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Mvc;
using SistemaInfo.ConnectUs.Api.Application.SistemaInfoServices.Infra;
using ActionFilterAttribute = System.Web.Http.Filters.ActionFilterAttribute;

namespace SistemaInfo.ConnectUs.Api.Application.Filters
{
    public class RequestLogFilter : ActionFilterAttribute
    {
        private readonly InfraExternalRepository _infraRepository;

        public RequestLogFilter()
        {
            _infraRepository = new InfraExternalRepository();
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            /*if (InfraExternalRepository.EnableLogService)
            {
                var attributes = actionContext.ActionDescriptor.GetCustomAttributes<IgnoreRequestLog>(true);
                if (attributes.Any())
                    return;
                
                var logBus = _infraRepository.LogConsumerRequest(actionContext);
                if (HttpContext.Current?.Items != null && logBus?.Id != null && logBus.Id != default(Guid))
                {
                    HttpContext.Current.Items.Add(SistemaInfoConsts.CurrentLogIdSessionKey, logBus.Id);
                    HttpContext.Current.Items.Add(SistemaInfoConsts.CurrentLogNivelSessionKey, logBus.Nivel ?? 0);
                }                
            }    */ 
            
            
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {                
            /*if (InfraExternalRepository.EnableLogService)
            {
                //Caso o atributo IgnoreRequestLog esteja listado no cabeçalho do método, então não permitimos que grave logs no ms
                var attributes = actionExecutedContext.ActionContext ActionDescriptor.GetCustomAttributes(true);
                if (attributes.Any(x => x.GetType() == typeof(IgnoreRequestLog)))
                    return;
                
                object sessionValue;
                if (HttpContext.Current?.Items != null &&
                    HttpContext.Current.Items.Contains(SistemaInfoConsts.CurrentLogIdSessionKey))
                {
                    sessionValue = HttpContext.Current.Items[SistemaInfoConsts.CurrentLogIdSessionKey];
                    if (sessionValue != null && !string.IsNullOrWhiteSpace(sessionValue.ToString()))
                    {
                        Guid requestId;
                        if (Guid.TryParse(sessionValue.ToString(), out requestId))
                            _infraRepository.LogConsumerResponse(actionExecutedContext, requestId);
                    }
                }
            }     */                  
        }
    }
}