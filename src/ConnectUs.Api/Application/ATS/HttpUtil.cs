using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Ajax.Utilities;
using SistemaInfo.ConnectUs.Api.Application.SistemaInfoServices;
using SistemaInfo.ConnectUs.Api.Application.SistemaInfoServices.Infra;
using SistemaInfo.Framework.Utils;

namespace SistemaInfo.ConnectUs.Api.Application.ATS
{
    public static class HttpUtil
    {
        private static readonly HttpClient client = new HttpClient();
        private static readonly InfraExternalRepository _infra = new InfraExternalRepository();

        public static string Send(string link, string json, Method post, string querystring = null)
        {
            var url = link;
            if (querystring.HasValue())
                url += "?" + querystring;

            var request = (HttpWebRequest) WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            if (post == Method.POST)
            {
                request.ContentType = "application/json";
                request.Method = "POST";

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
            }

            using (var response = (HttpWebResponse) request.GetResponse())
            using (var stream = response.GetResponseStream())
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }


        public static string Get(string link, string tokenMicroservices, string querystring = null)
        {
            var info = RequestLogUtils.GetLogInfo(HttpContext.Current, default(Guid), 0, tokenMicroservices);
            
            var log = _infra.LogPublishRequest(
                null, info, info.CurrentLogId, info.RootLogId, info.CurrentLogNivel, 
                ExtractEndPoint(link), string.Empty, ExtractMethod(link), ExtractQueryString(link),
                "POST", "application/json");
            
            var result = Send(link, querystring, Method.GET);

            _infra.LogPublishResponse(info, log.Id.GetValueOrDefault(), result, 200);
            
            return result;
        }

        public static string Post(string link, string json, string tokenMicroservices)
        {
            var info = RequestLogUtils.GetLogInfo(HttpContext.Current, default(Guid), 0, tokenMicroservices);
            
            var log = _infra.LogPublishRequest(
                json, info, info.CurrentLogId, info.RootLogId, info.CurrentLogNivel, 
                ExtractEndPoint(link), string.Empty, ExtractMethod(link), ExtractQueryString(link),
                "POST", "application/json");
            
            var result = Send(link, json, Method.POST);

            _infra.LogPublishResponse(info, log.Id.GetValueOrDefault(), result, 200);
            
            return result;
        }

        private static string ExtractEndPoint(string link)
        {
            var index = link.IndexOf('/', 10);
            if (index == -1)
                return link;

            return link.Substring(0, index);
        }

        private static string ExtractMethod(string link)
        {
            var index = link.IndexOf('/', 10);
            if (index == -1)
                return string.Empty;

            link = link.Substring(index + 1, link.Length - index - 1);
            index = link.IndexOf('?');
            if (index > -1)
                link = link.Remove(index, link.Length - index);
            return link;
        }

        private static string ExtractQueryString(string link)
        {
            var index = link.IndexOf('?');
            if (index == -1)
                return string.Empty;

            return link.Substring(index + 1, link.Length - index - 1);
        }
    }

    public enum Method
    {
        GET,
        POST,
        PUT,
        DELETE
    }
}