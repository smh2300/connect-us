namespace SistemaInfo.ConnectUs.Api.Application.ATS.Objects
{
    class EmpresaCacheData
    {
        public string Cnpj { get; set; }
        public string TokenAts { get; set; }
        public string TokenMicroServicos { get; set; }
    }
}