using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using SistemaInfo.ConnectUs.Api.Application.ATS.Objects;
using SistemaInfo.ConnectUs.Api.Application.Util;
using SistemaInfo.Framework.Utils;

namespace SistemaInfo.ConnectUs.Api.Application.ATS
{
    public class AtsRepository
    {        
        private static readonly List<EmpresaCacheData> EmpresaSingleton = new List<EmpresaCacheData>();

        private string _tokenMicroservices;

        public AtsRepository(string cnpjEmpresa)
        {
            _tokenMicroservices = GetTokenMicroServicos(cnpjEmpresa);
        }

        private EmpresaCacheData GetOrCreateEmpresaCache(string cnpj)
        {
            var result = EmpresaSingleton.FirstOrDefault(e => e.Cnpj == cnpj);

            if (result == null)
                lock (EmpresaSingleton)
                {
                    result = EmpresaSingleton.FirstOrDefault(e => e.Cnpj == cnpj);
                    if (result == null)
                    {
                        result = new EmpresaCacheData {Cnpj = cnpj};
                        EmpresaSingleton.Add(result);
                    }
                }

            return result;
        }                
        
        private string GetUrlAts()
        {
            return ConfigurationManager.AppSettings["ATS.Url"] ??
                   throw new Exception("AppSettings: ATS.Url não definido");
        }

        private string FormatUrl(string method, string queryString)
        {
            var result = GetUrlAts().SetEndChars("/");
            if (method.HasValue())
                result += method.RemoveStartValue("/");
            if (queryString.HasValue())
                result += "?" + queryString;
            return result;
        }
        
        public string GetTokenAts(string cnpjEmpresa)
        {
            var cached = GetOrCreateEmpresaCache(cnpjEmpresa);
            if (!string.IsNullOrWhiteSpace(cached.TokenAts))
                return cached.TokenAts;
            
            const string sql = @"   select ap.token
                                    from AUTENTICACAOAPLICACAO ap
                                         join EMPRESA E on ap.idempresa = E.idempresa
                                    where e.cnpj = @cnpj
                                    and   ap.cnpjaplicacao = @cnpj
                                    and   ap.ativo = 1";

            var result = AtsDb.SqlSimple(sql,
                new SqlParameter {ParameterName = "@cnpj", Value = cnpjEmpresa});

            if (result == null)
                throw new Exception("Token de autenticação no ATS não configurado para o CNPJ: " + cnpjEmpresa);

            cached.TokenAts = result.ToString();
            return cached.TokenAts;
        }

        public string GetTokenMicroServicos(string cnpjEmpresa)
        {
            var cached = GetOrCreateEmpresaCache(cnpjEmpresa);
            if (!string.IsNullOrWhiteSpace(cached.TokenMicroServicos))
                return cached.TokenMicroServicos;
            
            const string sql = @"select e.tokenmicroservices from EMPRESA e where e.cnpj = @cnpj";            
            var result = AtsDb.SqlSimple(sql, new SqlParameter("@cnpj", cnpjEmpresa));
            
            if (result == null)
                throw new Exception("Token de integração com micro serviço ATS não configurado para CNPJ: " + cnpjEmpresa);

            cached.TokenMicroServicos = result.ToString();
            return cached.TokenMicroServicos;
        }

        public string ConsultarMotorista(string cnpjEmpresa, string cpf)
        {
            const string method = "Pamcard/ConsultarMotoristaPorCPF";
//            const string Method = "Motorista/ConsultarPorCPF";
            const string args = "token={0}&CPFMotorista={1}&CNPJEmpresa={2}&CNPJAplicacao={3}";
            
            var url = FormatUrl(method, args.FormatEx(
                GetTokenAts(cnpjEmpresa), cpf, cnpjEmpresa, cnpjEmpresa));

            return HttpUtil.Get(url, _tokenMicroservices);
        }

        public string ConsultarProprietario(string cnpjEmpresa, string cnpj)
        {
            const string method = "Pamcard/ConsultarProprietario";
//            const string method = "Proprietario/Consultar";
            const string args = "token={0}&cnpjAplicacao={1}&cnpjcpfProprietario={2}";

            var url = FormatUrl(method,
                args.FormatEx(GetTokenAts(cnpjEmpresa), cnpjEmpresa, cnpj));

            return HttpUtil.Get(url, _tokenMicroservices);
        }
        
        public string ConsultarRntrc(string cnpjEmpresa, string rntrc)
        {
            const string method = "Proprietario/ConsultarPorRntrc";
            const string args = "token={0}&cnpjAplicacao={1}&rntrc={2}";

            var url = FormatUrl(method,
                args.FormatEx(GetTokenAts(cnpjEmpresa), cnpjEmpresa, rntrc));

            return HttpUtil.Get(url, _tokenMicroservices);
        }

        public string IntegrarMotorista(string json)
        {
            const string method = "Motorista/Integrar";            
            var url = FormatUrl(method, null);
            return HttpUtil.Post(url, json, _tokenMicroservices);
        }
        
        public string IntegrarProprietario(string json)
        {
            const string method = "Proprietario/Integrar";            
            var url = FormatUrl(method, null);
            return HttpUtil.Post(url, json, _tokenMicroservices);
        }

        public string IntegrarViagem(string json)
        {
            const string method = "Viagem/Integrar";            
            var url = FormatUrl(method, null);
            return HttpUtil.Post(url, json, _tokenMicroservices);
        }
        
        public string IntegrarCliente(string json)
        {
            const string method = "Cliente/Integrar";            
            var url = FormatUrl(method, null);
            return HttpUtil.Post(url, json, _tokenMicroservices);
        }
        
        public string IntegrarVeiculo(string json)
        {
            const string method = "Veiculo/Integrar";            
            var url = FormatUrl(method, null);
            return HttpUtil.Post(url, json, _tokenMicroservices);
        }
        
        public string ConsultarFrota(string json)
        {
            const string method = "Veiculo/ConsultarFrotaTransportador";
            var url = FormatUrl(method, null);
            return HttpUtil.Post(url, json, _tokenMicroservices);
        }
        
        public string CancelarViagem(string cnpjEmpresa, long idViagem)
        {
            const string method = "Viagem/CancelarViagem";
            const string args = "Token={0}&CnpjAplicacao={1}&CodigoViagem={2}";
            
            var url = FormatUrl(method,
                args.FormatEx(GetTokenAts(cnpjEmpresa), cnpjEmpresa, idViagem));
            
            return HttpUtil.Post(url, null, _tokenMicroservices);
        }
        
        public string ConsultarViagem(string json)
        {
            const string method = "Viagem/Consultar";
            var url = FormatUrl(method, null);
            return HttpUtil.Post(url, json, _tokenMicroservices);
        }
        
        public string BaixarEvento(string json)
        {
            const string method = "Viagem/BaixarEvento";            
            var url = FormatUrl(method, null);
            return HttpUtil.Post(url, json, _tokenMicroservices);
        }
        
        public string ConsultarCustoPedagioRota(string json)
        {
            const string method = "Viagem/ConsultarCustoPedagioRota";            
            var url = FormatUrl(method, null);
            return HttpUtil.Post(url, json, _tokenMicroservices);
        }
        
        public string UpdateValuesFreightContract(string json)
        {
            const string method = "Viagem/AlterarValoresViagem";            
            var url = FormatUrl(method, null);
            return HttpUtil.Post(url, json, _tokenMicroservices);
        }
        
        public string UpdateParcelStatus(string json)
        {
            const string method = "Viagem/AlterarStatusParcelas";            
            var url = FormatUrl(method, null);
            return HttpUtil.Post(url, json, _tokenMicroservices);
        }
        
        public string InserirParcela(string json)
        {
            const string method = "Viagem/AdicionarEventosViagem";            
            var url = FormatUrl(method, null);
            return HttpUtil.Post(url, json, _tokenMicroservices);
        }
        
        public string FindParcelStatus(string json)
        {
            const string method = "Viagem/ConsultarDetalhesEvento";                        
            var url = FormatUrl(method, null);            
            return HttpUtil.Post(url, json, _tokenMicroservices);            
        }
        
        public string InsertCard(string json)
        {
            const string method = "Cartoes/VincularCartao";                        
            var url = FormatUrl(method, null);            
            return HttpUtil.Post(url, json, _tokenMicroservices);            
        }
    }
}