namespace SistemaInfo.ConnectUs.Functions.Utils
{
    public static class StringUtils
    {
        public static string NullIfEmpty(this string value)
        {
            return string.IsNullOrWhiteSpace(value) ? null : value;
        }
    }
}