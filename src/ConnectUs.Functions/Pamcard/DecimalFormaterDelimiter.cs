﻿using System;
using System.Globalization;
using System.Linq;
using SysMiddle.ConnectUs.Core.Interface.ValueObject.Rule.Child;
using SysMiddle.ConnectUs.Core.Interface.ValueObject.Rule.Parent;

namespace SistemaInfo.ConnectUs.Functions.Pamcard
{
    
    // ReSharper disable once UnusedMember.Global
    public class DecimalFormaterDelimiter : FunctionMember
    {
        public override string Name => "DecimalFormaterDelimiter";
        public override string OwnerName => "Sistema Info";
        
        public override void MakeSignature()
        {
            var signatureFunction = new SignatureFunction
            {
                Description = "Formatar um valor decimal com número de casas decimais e delimitador ponto ou vírgula",
                ReturnDescription = "Valor decimal formatado como string",
                ReturnType = TypeEnum.Object
            };

            signatureFunction.AddParameter(new RuleParameterFunctionVO
            {
                Name = "valor",
                Description = "valor decimal",
                Type = TypeEnum.Decimal
            });
            
            signatureFunction.AddParameter(new RuleParameterFunctionVO
            {
                Name = "decimais",
                Description = "quantidade de casas decimais, caso vazio = 2",
                Type = TypeEnum.Int32
            });
            
            signatureFunction.AddParameter(new RuleParameterFunctionVO
            {
                Name = "delimitadorSaida",
                Description = "delimitador do valor na saida '.' ou ',', caso vazio = '.'",
                Type = TypeEnum.Char
            });

            AddNewSignature(signatureFunction);
        }

        public override object Execute(object[] pars)
        {
            try
            {
                if (pars.ElementAtOrDefault(0) == null || string.IsNullOrEmpty(pars[0].ToString()))
                    return null;
                
                var valor = decimal.Parse(pars[0].ToString(), CultureInfo.CurrentCulture.NumberFormat);
                
                var delimitadorSaida = pars.ElementAtOrDefault(2) != null ? pars[2].ToString() : ".";
                string cultureSaida = delimitadorSaida.Equals(",") ? "pt-BR" : "en-US";
                
                NumberFormatInfo numberFormatSaida = new CultureInfo(cultureSaida, false).NumberFormat;
                numberFormatSaida.NumberGroupSeparator = string.Empty;
                
                var qtdeDecimais = pars.ElementAtOrDefault(1) != null ? Convert.ToInt32(pars[1].ToString()) : 2;
                numberFormatSaida.NumberDecimalDigits = qtdeDecimais;

                return valor.ToString("N", numberFormatSaida);
            }
            catch (Exception e)
            {
                Log.Error("Erro ao executar DecimalFormaterDelimiter: " + e.Message);
                Log.Error("StackTrace: " + e.StackTrace);
                return e.Message;
            }
        }        
    }
}