﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Xml.Serialization;
using Microsoft.SqlServer.Server;
using SistemaInfo.ConnectUs.Functions.Utils;

namespace SistemaInfo.ConnectUs.Functions.Pamcard
{
    public class PamcardDataManager
    {
        private Envelope _requestEnvelope = null;

        public PamcardDataManager()
        {            
        }
        
        public PamcardDataManager(Envelope requestEnvelope)
        {
            _requestEnvelope = requestEnvelope;            
        }

        /// <summary>
        /// Nome do método da parcard para executar
        /// </summary>
        public string RequestContext => _requestEnvelope?.Body?.execute?.arg0?.context;

        public arg0Fields[] GetFields() => _requestEnvelope?.Body?.execute?.arg0?.fields;

        #region GetFieldVale / GetFieldValueIf
        
        // String
        public string GetFieldValue(string key, bool nullIfEmpty = true)
        {
            return nullIfEmpty
                ? GetFields()?.FirstOrDefault(a => a.key == key)?.value.NullIfEmpty()
                : GetFields()?.FirstOrDefault(a => a.key == key)?.value;
        }

        public string GetFieldValueIf(string resultKeyValue, string findKey, object findValue, params int[] indexMaxValues)
        {
            return GetFieldValueIf(resultKeyValue, findKey, findValue, true, indexMaxValues);
        }

        public string GetFieldValueIf(string resultKeyValue, string findKey, object findValue, bool nullIfEmpty, params int[] indexMaxValues)
        {
            var findValueStr = findValue?.ToString();
            
            Tuple<bool, string> InternalFind(IEnumerable<int> currentIndexes, IEnumerable<int> children)
            {
                // Se não tem mais nenhum filho, já está no ponto final 
                /*if (children.Length == 0)
                {
                    var key = string.Format(findKey, currentIndexes);
                    return GetFieldValue(findKey, nullIfEmpty);
                }*/
                if (!children.Any())
                    return new Tuple<bool, string>(false, null);
                
                int max = children.First();
                
                // Criar nova lista de indices filhos, removendo o primeiro filho que já foi inserido na lista pai
                var newChildrenIndexes = new List<int>(children);
                newChildrenIndexes.RemoveAt(0);
                
                // Executar varredura
                for (var i = 1; i <= max; i++)
                {
                    // Criar nova lista de indices atuais, adicionando o indice do loop ao final
                    var newCurrentIndexes = new List<int>(currentIndexes);
                    newCurrentIndexes.Add(i);
                                        
                    if (newChildrenIndexes.Count > 0)
                    {
                        // Se ainda haver mais filhos, executa recursivamente esta rotina
                        var result = InternalFind(newCurrentIndexes.ToArray(), newChildrenIndexes);
                        if (result.Item1)
                            return result;
                    }
                    else
                    {
                        // Se não houver filho significa que está no nível final para verificação
                        var currentIndexesStringArray = newCurrentIndexes.Select(x => x.ToString()).ToArray();
                        
                        var fKey = string.Format(findKey, currentIndexesStringArray);
                        var fKeyValue = GetFieldValue(fKey, nullIfEmpty);
                        if (fKeyValue != null &&
                            string.Equals(fKeyValue, findValueStr, StringComparison.InvariantCultureIgnoreCase))
                        {
                            var rKey = string.Format(resultKeyValue, currentIndexesStringArray);
                            return new Tuple<bool, string>(true, GetFieldValue(rKey, nullIfEmpty));
                        }
                    }
                }

                return new Tuple<bool, string>(false, null);
            }

            return InternalFind(new int[] { }, indexMaxValues)?.Item2;            
        }

        // Int
        public int GetFieldValueInt(string key, int @default = 0)
        {
            var value = GetFieldValue(key);
            if (string.IsNullOrWhiteSpace(value))
                return @default;

            if (!int.TryParse(value, out var result))
                return @default;

            return result;
        }
        
        // Int64
        public long GetFieldValueInt64(string key, long @default = 0)
        {
            var value = GetFieldValue(key);
            if (string.IsNullOrWhiteSpace(value))
                return @default;

            if (!long.TryParse(value, out var result))
                return @default;

            return result;
        }
        
        #endregion
        
        public void LoadXml(string xml)
        {
            using (var input = new MemoryStream())
            {
                var buffer = Encoding.UTF8.GetBytes(xml);
                input.Write(buffer, 0, buffer.Length);
                input.Seek(0, SeekOrigin.Begin);

                // Se for XML de requisição
                var serializer = new XmlSerializer(typeof(Envelope));
                _requestEnvelope = (Envelope) serializer.Deserialize(input);
                        
                // Se for XML de resposta                                                                       
            }
        }
        
        public static PamcardDataManager CreateFromRequestXml(string xml)
        {
            var result = new PamcardDataManager();
            result.LoadXml(xml);
            return result;            
        }
    }
}