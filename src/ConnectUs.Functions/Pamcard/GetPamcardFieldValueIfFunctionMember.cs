﻿using System;
using System.Collections.Generic;
using SysMiddle.ConnectUs.Core.Interface.ValueObject.Rule.Child;
using SysMiddle.ConnectUs.Core.Interface.ValueObject.Rule.Parent;

namespace SistemaInfo.ConnectUs.Functions.Pamcard
{
    // ReSharper disable once UnusedMember.Global
    public class GetPamcardFieldValueIfFunctionMember : FunctionMember
    {
        public override string Name => "GetPamcardFieldValueIf";
        public override string OwnerName => "Sistema Info";
        
        public override void MakeSignature()
        {
            var signatureFunction = new SignatureFunction
            {
                Description = "Obter valor do campo com a chave especificada quando o valor de outra chave satisfazer a condição de igualdade",
                ReturnDescription = "Valor configurado no XML da Pamcard",
                ReturnType = TypeEnum.String
            };
            
            signatureFunction.AddParameter(new RuleParameterFunctionVO
            {
                Name = "dataAccessManager",
                Description = "Manipulador de acesso ao XML pamcard retornado pela função LoadPamcardRequestXml",
                Type = TypeEnum.Object
            });

            signatureFunction.AddParameter(new RuleParameterFunctionVO
            {
                Name = "resultKeyValue",
                Description = "Chave da Pamcard para retornar valor.\n" +
                              "Exemplo: viagem.favorecido1.documento{0}.numero => Retornar o número de algum documento que satisfazer a condição de igualdade pelo campo do parâmetro 'findKey'.",
                Type = TypeEnum.String
            });
            
            signatureFunction.AddParameter(new RuleParameterFunctionVO
            {
                Name = "findKey",
                Description = "Chave da Pamcard buscar valore de igualdade.\n" +
                              "Exemplo: viagem.favorecido1.documento{0}.tipo => Para retornar o número de documento onde a chave tipo satisfazer a condição de igualdade.",
                Type = TypeEnum.String
            });
            
            signatureFunction.AddParameter(new RuleParameterFunctionVO
            {
                Name = "findValue",
                Description = "Valor para verificar condição de igualdade com o a tag informada em 'findKey'.\n" +
                              "Constante para verificar, exemplo: 2 => Para retornar o número de documento onde o valor da chave tipo for 2.",
                Type = TypeEnum.String
            });
                        
            signatureFunction.AddParameter(new RuleParameterFunctionVO
            {
                Name = "nullIfEmpty",
                Description = "Retornar nulo caso o valor da chave for string vazia ou espaço em branco (Default true)",
                Type = TypeEnum.Boolean
            });
            
            signatureFunction.AddParameter(new RuleParameterFunctionVO
            {
                Name = "indexMaxValues",
                Description = "Array de inteiro com os limites a percorrer em cada loop de campo. Exemplo: Quantidade máxima de favorecidos / quantidade máxima de documentos por favorecido",
                Type = TypeEnum.ByteArray
            });

            AddNewSignature(signatureFunction);            
        }

        public override object Execute(object[] pars)
        {
            try
            {
                // 0-dataAccessManager
                // 1-resultKeyValue
                // 2-findKey
                // 3-findValue
                // 4-nullIfEmpty
                // 5..N-indexMaxValues
                if (pars.Length > 5)
                {
                    var manager = pars[0] as PamcardDataManager;
                    if (manager == null)
                        throw new ArgumentNullException("dataAccessManager", "Gerenciador de acesso a XML Pamcard não informado: " + pars[0] + " ==> " + pars[0]?.GetType()?.FullName);
                        
                    var resultKeyValue = pars[1].ToString();
                    var findKey = pars[2].ToString();
                    var findValue = pars[3].ToString();
                    var nullIfEmpty = Convert.ToBoolean(pars[4].ToString());
                    var indexMaxValues = new List<int>();
                    
                    if (string.IsNullOrWhiteSpace(resultKeyValue))
                        throw new ArgumentNullException("resultKeyValue", "Chave para obter valor de propriedade XML Pamcard não informada");
                    
                    if (string.IsNullOrWhiteSpace(findKey))
                        throw new ArgumentNullException("findKey", "Chave para comparar valores e obter resultado de propriedade XML Pamcard não informada");

                    for (int i = 5; i < pars.Length; i++)
                        indexMaxValues.Add(Convert.ToInt32(pars[i]));
                    
//                    Log.Debug($"Obtendo campo: {key}. Fields: " + (manager.GetFields()?.Length ?? 0));

                    return manager.GetFieldValueIf(
                        resultKeyValue, findKey, findValue, nullIfEmpty, indexMaxValues.ToArray());
                }

                return null;
            }
            catch (Exception e)
            {
                Log.Error("Erro ao ler campo key/value com condição XML Pamcard: " + e.Message);
                Log.Error("StackTrance: " + e.StackTrace);
                throw;
            }
        }                
    }
}