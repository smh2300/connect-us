﻿using System;
using SysMiddle.ConnectUs.Core.Interface.ValueObject.Rule.Child;
using SysMiddle.ConnectUs.Core.Interface.ValueObject.Rule.Parent;

namespace SistemaInfo.ConnectUs.Functions.Pamcard
{
    // ReSharper disable once UnusedMember.Global
    public class GetPamcardFieldValueFunctionMember : FunctionMember
    {
        public override string Name => "GetPamcardFieldValue";
        public override string OwnerName => "Sistema Info";
        
        public override void MakeSignature()
        {
            var signatureFunction = new SignatureFunction
            {
                Description = "Obter valor do campo com a chave especificada",
                ReturnDescription = "Valor configurado no XML da Pamcard",
                ReturnType = TypeEnum.String
            };
            
            signatureFunction.AddParameter(new RuleParameterFunctionVO
            {
                Name = "dataAccessManager",
                Description = "Manipulador de acesso ao XML pamcard retornado pela função LoadPamcardRequestXml",
                Type = TypeEnum.Object
            });

            signatureFunction.AddParameter(new RuleParameterFunctionVO
            {
                Name = "key",
                Description = "Chave da Pamcard para buscar valor",
                Type = TypeEnum.String
            });
            
            signatureFunction.AddParameter(new RuleParameterFunctionVO
            {
                Name = "nullIfEmpty",
                Description = "Retornar nulo caso o valor da chave for string vazia ou espaço em branco (Default true)",
                Type = TypeEnum.Boolean
            });

            AddNewSignature(signatureFunction);
        }

        public override object Execute(object[] pars)
        {
            try
            {
                // 0-dataAccessManager
                // 1-key
                // 2-nullIfEmpty
                if (pars.Length >= 2)
                {
                    var manager = pars[0] as PamcardDataManager;
                    if (manager == null)
                        throw new ArgumentNullException("dataAccessManager", "Gerenciador de acesso a XML Pamcard não informado: " + pars[0] + " ==> " + pars[0]?.GetType()?.FullName);
                        
                    var key = pars[1]?.ToString();
                    if (string.IsNullOrWhiteSpace(key))
                        throw new ArgumentNullException("key", "Chave para obter valor de propriedade XML Pamcard não informada");

                    var nullIfEmpty = true;
                    if (pars.Length > 2)
                        nullIfEmpty = Convert.ToBoolean(pars[2]);

//                    Log.Debug($"Obtendo campo: {key}. Fields: " + (manager.GetFields()?.Length ?? 0));

                    return manager.GetFieldValue(key, nullIfEmpty);
                }

                return null;
            }
            catch (Exception e)
            {
                Log.Error("Erro ao ler campo key/value XML Pamcard: " + e.Message);
                Log.Error("StackTrance: " + e.StackTrace);
                throw;
            }
        }                
    }
}