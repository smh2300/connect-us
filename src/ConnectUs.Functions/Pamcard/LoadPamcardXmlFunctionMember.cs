﻿using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using SysMiddle.ConnectUs.Core.Interface.ValueObject.Rule.Child;
using SysMiddle.ConnectUs.Core.Interface.ValueObject.Rule.Parent;

namespace SistemaInfo.ConnectUs.Functions.Pamcard
{
    
    // ReSharper disable once UnusedMember.Global
    public class LoadPamcardXmlFunctionMember : FunctionMember
    {
        public override string Name => "LoadPamcardXml";
        public override string OwnerName => "Sistema Info";
        
        public override void MakeSignature()
        {
            var signatureFunction = new SignatureFunction
            {
                Description = "Carregar XML de requisição/resposta da Pamcard e retornar objeto com a referência ao manipulador de propriedades",
                ReturnDescription = "Manipulador de acesso a propriedades da Pamcard",
                ReturnType = TypeEnum.Object
            };

            signatureFunction.AddParameter(new RuleParameterFunctionVO
            {
                Name = "xml",
                Description = "Layout XML de requisição da Pamcard",
                Type = TypeEnum.String
            });

            AddNewSignature(signatureFunction);
        }

        public override object Execute(object[] pars)
        {
            try
            {
                //Log.Debug("Carregando XML pamcard");
                
                if (pars.Length > 0)
                {
                    var xml = pars[0].ToString();
                    return PamcardDataManager.CreateFromRequestXml(xml);
                }

                throw new ArgumentException("XML para carregar gerenciador de campos Pamcard não informado.");
            }
            catch (Exception e)
            {
                Log.Error("Erro ao carregar layout XML Pamcard: " + e.Message);
                Log.Error("StackTrance: " + e.StackTrace);
                return e.Message;
            }
        }        
    }
}