﻿using System;
using SysMiddle.ConnectUs.Core.Interface.ValueObject.Rule.Child;
using SysMiddle.ConnectUs.Core.Interface.ValueObject.Rule.Parent;

namespace SistemaInfo.ConnectUs.Functions.Pamcard
{
    
    // ReSharper disable once UnusedMember.Global
    public class FirstNotEmptyFunctionMember : FunctionMember
    {
        public override string Name => "FirstNotEmpty";
        public override string OwnerName => "Sistema Info";
        
        public override void MakeSignature()
        {
            var signatureFunction = new SignatureFunction
            {
                Description = "Obter primeiro argumento diferente de null e não for uma string vazia",
                ReturnDescription = "Obter primeiro argumento diferente de null e não for uma string vazia",
                ReturnType = TypeEnum.Object
            };

            signatureFunction.AddParameter(new RuleParameterFunctionVO
            {
                Name = "values",
                Description = "object[] array de valores",
                Type = TypeEnum.Object
            });

            AddNewSignature(signatureFunction);
        }

        public override object Execute(object[] pars)
        {
            try
            {
                foreach (var item in pars)
                {
                    if (item != null)
                    {
                        if (item is string)
                        {
                            if (!string.IsNullOrWhiteSpace(item.ToString()))
                                return item;
                        }

                        return item;
                    }
                }

                return null;
            }
            catch (Exception e)
            {
                Log.Error("Erro ao execut IfEmpty: " + e.Message);
                Log.Error("StackTrance: " + e.StackTrace);
                return e.Message;
            }
        }        
    }
}