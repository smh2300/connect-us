using SysMiddle.ConnectUs.Core.Interface.ValueObject.Rule.Parent;

class Teste : FunctionMember
    {
        public override void MakeSignature()
        {
            throw new NotImplementedException();
        }

        public override object Execute(object[] pars)
        {
            throw new NotImplementedException();
        }

        public override string Name { get; }
        public override string OwnerName { get; }
    }


Exemplo: 

 var signatureFunction = new SignatureFunction
            {
                Description = "Descrição do metodo",
                ReturnDescription = "Retorno do metodo",
                ReturnType = TypeEnum.String//tipo de retorno
            };

            signatureFunction.AddParameter(new RuleParameterFunctionVO
            {
                Name = "Nome do parametro",
                Description = "Descrição do valor",
                Type = TypeEnum.String//tipo de parametro
            });

            AddNewSignature(signatureFunction);

O detalhamento da função que aparece no intellisense na regra

Exemplo do método execute de um split por exemplo

public override object Execute(object[] pars)
        {
            if (pars.Length > 0x1)
            {
                string value = pars[0x0]?.ToString(), delimiter = pars[0x1]?.ToString();

                if (value == null || string.IsNullOrEmpty(delimiter)) return null;

                return value.Split(delimiter[0x0]);
            }

            return null;
        }