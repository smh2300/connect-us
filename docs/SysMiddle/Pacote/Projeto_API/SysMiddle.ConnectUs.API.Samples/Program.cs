﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SysMiddle.ConnectUs.API.Service;

namespace SysMiddle.ConnectUs.API.Samples
{
    class Program
    {
        /// <summary>
        /// Para primeira execução, favor configurar os arquivos 'global.config' e 'logger.xml'
        /// 
        /// Vale ressaltar que no arquivo 'global.config', o usuário pode realizar a configuração para busca de informações em banco de dados ou num arquivo de back-up.
        /// OBS: Favor alterar o código de licença.
        /// </summary>

        static void Main(string[] args)
        {
            try
            {
                #region Api Manager Methods

                var projects = APIManager.Instance.GetAvailableProjects();

                //Carregamento da classe API Executor através do identificador do projeto, ao qual a partir desta instância é possível realizar os processos de parser e mapeamento de informações.
                var apiExecutor =
                    APIManager.Instance.GetApiExecutorByIdentifier("Sistema Info",
                        "0f3fcd8a-871e-49b2-9841-e59da23750f7");

                #endregion

                //Classe responsável pela execução dos processos de parser e mapeamento

                #region Api Executor Methods

                var layouts = apiExecutor.GetLayouts();
                var mappers = apiExecutor.GetMappers();

                var layoutByGuid = apiExecutor.GetLayoutByIdentifier("LAY_b73211d4-22c3-40ce-910f-d04aeff2aafb");
                var layoutByName = apiExecutor.GetLayoutByName("Layout_Xml_NFe_2.00");

                var mapperByGuid = apiExecutor.GetMapperByIdentifier("MAP_d97f38d1-6a4c-4c24-b09a-fafb0de301b1");
                var mapperByName = apiExecutor.GetMapperByName("Mapeador_Xml_x_TextoDelimitado");


                //Execução do Parser é responsável por realizar a validação de um Layout e em caso de inconsistências informar detalhamente as mesmas.
                var nfeContent = File.ReadAllText("NF-e 2.00.xml");

                var parserContext = apiExecutor.ExecuteParser(layoutByName, nfeContent);

                var valuesByIdentifier = apiExecutor.GetValuesByElementIdentifier(parserContext.ParsedLayout.Elements,
                    "TAG_f3689be3-51d5-4392-b8fb-a47fa4cd618e");

                var valuesByName = apiExecutor.GetValuesByElementName(parserContext.ParsedLayout.Elements, "CNPJ");

                var valuesByXPath = apiExecutor.GetValuesByElementXPath(parserContext.ParsedLayout.Elements,
                    "nfeProc/NFe/infNFe/ide/cNF");


                //Execução do mapeamento é responsável pela transformação de dados entre Layouts de entrada e saída.
                //Caso o Layout de entrada do mapeador seja do formato Banco de Dados, o parâmetro contentMessage pode ser passado como Nulo ou Vazio.
                var xmlFileContent = File.ReadAllText("testMapperIn.xml");

                var mapperResultBasic =
                    apiExecutor.ExecuteMapper("MAP_d97f38d1-6a4c-4c24-b09a-fafb0de301b1", xmlFileContent);

                if (mapperResultBasic != null)
                {
                    var valuesByIdentifierByMapper = apiExecutor.GetValuesByElementIdentifier(
                        mapperResultBasic.SourceLayoutResult.Elements, "TAG_fafc4d38-18e0-406f-8b06-7cb49178016f");
                    var valuesByNameByMapper =
                        apiExecutor.GetValuesByElementName(mapperResultBasic.SourceLayoutResult.Elements, "Value");
                    var valuesByXPathByMapper =
                        apiExecutor.GetValuesByElementXPath(mapperResultBasic.SourceLayoutResult.Elements,
                            "Input/Values/Value");
                }

                #endregion
            }
            catch (Exception e)
            {                
                Console.WriteLine("Error: " + e.Message);
                Console.ReadLine();
            }
        }
    }
}
