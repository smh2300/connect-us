@echo off

:: Forçar mudança de diretório atual para o atual do arquivo .bat
cd /D "%~dp0"

echo EXECUTAR COMO ADMINISTRADOR - EXPORTAR E ATUALIZAR IIS
echo ---------------------------

set outputdir=C:\Versao\ConnectUs
set poolPrefix=
set iisRootDir=

set msbuildpath=C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\MSBuild\15.0\Bin\MSBuild.exe

echo Diretorio de exportacao: %outputdir%
echo ---------------------------

:: SVN Update
set /p svnupdate= SVN Update [S/N] (Default=S): 
if "%svnupdate%"=="" (set svnupdate=S)
if "%svnupdate%"=="S" (
	svn update ..\	
	echo Atualizacao de fontes concluida
	echo ---------------------------	
)
echo.

:: Escolhendo projeto para exportar
echo PROJETOS
echo 0-Todos
echo 1-ATS.WS
echo 2-ATS.UI.Web
set /p opcao="Opcao para atualizar (Default=1-ATS.WS): "
if "%opcao%"=="" (set opcao=1)

set /p iis="Atualizar IIS [S/N] (Default=N): "
if "%iis%"=="" (set iis=N)

set /p poolPrefix="Nome instancia aplicacao (Default=ATS.Extratta.Hml): "
	
echo ---------------------------
set ws=false
set web=false

:: Transformando input do usuario nas variaveis de controle
if %opcao%==0 (
	set ws=true
	set web=true)
if %opcao%==1 set ws=true
if %opcao%==2 set web=true

if "%poolPrefix%"=="" (
	set poolPrefix=ATS.Extratta.Hml)
	
if "%poolPrefix%" neq "" (
	set iisRootDir=C:\IIS\%poolPrefix%\)

:: Imprimindo configuracoes do usuario
echo CONFIGURACOES
echo Opcao: %opcao%
echo ATS.WS: %ws%
echo ATS.UI.Web: %web%
echo.
echo Atualizar IIS: %iis%
echo Prefixo Pool: %poolPrefix%
echo Pasta atualizacao: %iisRootDir%
echo.
echo Output build folder: %outputdir%

:: Excluíndo arquivos atualmente na pasta destino da publicação
echo ---------------------------
echo Limpando diretorio
if %ws%==true (@RD /S /Q "%outputdir%\WS")
if %web%==true (@RD /S /Q "%outputdir%\Web")

:: Publicar projetos como release
echo ---------------------------
echo Exportando projetos
if %ws%==true ("%msbuildpath%" "..\src\ConnectUs.Api\ConnectUs.Api.csproj" "/p:Platform=AnyCPU;Configuration=Release;PublishDestination=%outputdir%\WS" /t:PublishToFileSystem)

:: Remover arquivos web.confi
echo ---------------------------

echo.
echo Excluindo Web.config
del /q /f %outputdir%\WS\Web*.config
del /q /f %outputdir%\Web\Web*.config

:: Atualizando IIS
if %iis%==S (
	echo Atualizando IIS
	
	c:
	cd c:\Windows\System32\inetsrv
	
	:: Atualizando Extratta	
	if %ws%==true (
		if exist "%iisRootDir%\WS" (
			appcmd stop apppool /apppool.name:"%poolPrefix%.WS"
			xcopy "%outputdir%\WS" "%iisRootDir%\WS" /s/h/e/k/f/c/y
			appcmd start apppool /apppool.name:"%poolPrefix%.WS"
		)
		
		if exist "%iisRootDir%\WS_Api" (
			appcmd stop apppool /apppool.name:"%poolPrefix%.WS_Api"
			xcopy "%outputdir%\WS" "%iisRootDir%\WS_Api" /s/h/e/k/f/c/y
			appcmd start apppool /apppool.name:"%poolPrefix%.WS_Api"
		)
		
		if exist "%iisRootDir%\WS_Front" (
			appcmd stop apppool /apppool.name:"%poolPrefix%.WS_Front"
			xcopy "%outputdir%\WS" "%iisRootDir%\WS_Front" /s/h/e/k/f/c/y
			appcmd start apppool /apppool.name:"%poolPrefix%.WS_Front"
		)
		
		if exist "%iisRootDir%\WS_Mobile" (
			appcmd stop apppool /apppool.name:"%poolPrefix%.WS_Mobile"
			xcopy "%outputdir%\WS" "%iisRootDir%\WS_Mobile" /s/h/e/k/f/c/y
			appcmd start apppool /apppool.name:"%poolPrefix%.WS_Mobile"
		)			
	)	

	if %web%==true (
		appcmd stop apppool /apppool.name:"%poolPrefix%.Web"
		xcopy "%outputdir%\Web" "%iisRootDir%\Web" /s/h/e/k/f/c/y
		appcmd start apppool /apppool.name:"%poolPrefix%.Web"
	)	
)

echo ---------------------------
echo Processo concluido

pause